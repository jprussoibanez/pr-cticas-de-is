Feature: Play options blackjack
In order to play a round
As a user of the blackjack API
I want to play a hit or stand

Scenario: Play a hit
Given a new deck card
Given a new player
When next card is suit spades number 2
When next card is suit spades number 5
When I start game
When next card is suit spades number 7
When I play hit
Then I have card suit spades number 7
Then I have card suit spades number 2
Then I have card suit spades number 5

Scenario: Play a stand
Given a new deck card
Given a new player
When next card is suit spades number 2
When next card is suit spades number 5
When I start game
When I play stand
Then I have card suit spades number 2
Then I have card suit spades number 5