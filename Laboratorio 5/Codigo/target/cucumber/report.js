$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri('ort\practicasIS\laboratorio5\test\french_deck.feature');
formatter.feature({
  "id": "french-deck-cards",
  "description": "In order to play blackjack\r\nAs a user of the blackjack API\r\nI want to use a french deck card to play",
  "name": "French deck cards",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "french-deck-cards;get-all-cards-in-deck",
  "description": "",
  "name": "Get all cards in deck",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "a new deck card",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I get all cards in deck",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "I get card suit spades number Ace",
  "keyword": "Then ",
  "line": 9
});
formatter.step({
  "name": "I get card suit spades number 2",
  "keyword": "Then ",
  "line": 10
});
formatter.step({
  "name": "I get card suit spades number Jack",
  "keyword": "Then ",
  "line": 11
});
formatter.step({
  "name": "I get card suit spades number Queen",
  "keyword": "Then ",
  "line": 12
});
formatter.step({
  "name": "I get card suit spades number King",
  "keyword": "Then ",
  "line": 13
});
formatter.step({
  "name": "I get card suit clubs number Ace",
  "keyword": "Then ",
  "line": 14
});
formatter.step({
  "name": "I get card suit diamonds number Ace",
  "keyword": "Then ",
  "line": 15
});
formatter.step({
  "name": "I get card suit hearts number Ace",
  "keyword": "Then ",
  "line": 16
});
formatter.match({
  "location": "BlackjackStepsTest.newDeckCard()"
});
formatter.result({
  "duration": 130115122,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.getAllCardsInDeck()"
});
formatter.result({
  "duration": 45037,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 16
    },
    {
      "val": "Ace",
      "offset": 30
    }
  ],
  "location": "BlackjackStepsTest.getCard(String,String)"
});
formatter.result({
  "duration": 2785368,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 16
    },
    {
      "val": "2",
      "offset": 30
    }
  ],
  "location": "BlackjackStepsTest.getCard(String,String)"
});
formatter.result({
  "duration": 43112,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 16
    },
    {
      "val": "Jack",
      "offset": 30
    }
  ],
  "location": "BlackjackStepsTest.getCard(String,String)"
});
formatter.result({
  "duration": 40033,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 16
    },
    {
      "val": "Queen",
      "offset": 30
    }
  ],
  "location": "BlackjackStepsTest.getCard(String,String)"
});
formatter.result({
  "duration": 51195,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 16
    },
    {
      "val": "King",
      "offset": 30
    }
  ],
  "location": "BlackjackStepsTest.getCard(String,String)"
});
formatter.result({
  "duration": 35799,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "clubs",
      "offset": 16
    },
    {
      "val": "Ace",
      "offset": 29
    }
  ],
  "location": "BlackjackStepsTest.getCard(String,String)"
});
formatter.result({
  "duration": 53121,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "diamonds",
      "offset": 16
    },
    {
      "val": "Ace",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.getCard(String,String)"
});
formatter.result({
  "duration": 56970,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "hearts",
      "offset": 16
    },
    {
      "val": "Ace",
      "offset": 30
    }
  ],
  "location": "BlackjackStepsTest.getCard(String,String)"
});
formatter.result({
  "duration": 57740,
  "status": "passed"
});
formatter.scenario({
  "id": "french-deck-cards;take-out-card-played",
  "description": "",
  "name": "Take out card played",
  "keyword": "Scenario",
  "line": 18,
  "type": "scenario"
});
formatter.step({
  "name": "a new deck card",
  "keyword": "Given ",
  "line": 19
});
formatter.step({
  "name": "I play card suit spades number Ace",
  "keyword": "When ",
  "line": 20
});
formatter.step({
  "name": "card suit spades number Ace is not in deck",
  "keyword": "Then ",
  "line": 21
});
formatter.match({
  "location": "BlackjackStepsTest.newDeckCard()"
});
formatter.result({
  "duration": 169755,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 17
    },
    {
      "val": "Ace",
      "offset": 31
    }
  ],
  "location": "BlackjackStepsTest.playCard(String,String)"
});
formatter.result({
  "duration": 59665,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 10
    },
    {
      "val": "Ace",
      "offset": 24
    }
  ],
  "location": "BlackjackStepsTest.cardNotInDeck(String,String)"
});
formatter.result({
  "duration": 55815,
  "status": "passed"
});
formatter.uri('ort\practicasIS\laboratorio5\test\game_result.feature');
formatter.feature({
  "id": "play-blackjack-game",
  "description": "In order to play blackjack\r\nAs a user of the blackjack API\r\nI want to get the game result",
  "name": "Play blackjack game",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "play-blackjack-game;dealer-over-21",
  "description": "",
  "name": "Dealer over 21",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "a new deck card",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "a new player",
  "keyword": "Given ",
  "line": 8
});
formatter.step({
  "name": "next card is suit spades number 9",
  "keyword": "When ",
  "line": 9
});
formatter.step({
  "name": "next card is suit spades number Jack",
  "keyword": "When ",
  "line": 10
});
formatter.step({
  "name": "I start game",
  "keyword": "When ",
  "line": 11
});
formatter.step({
  "name": "next card is suit spades number 7",
  "keyword": "When ",
  "line": 12
});
formatter.step({
  "name": "I play hit",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "I lose game",
  "keyword": "Then ",
  "line": 14
});
formatter.match({
  "location": "BlackjackStepsTest.newDeckCard()"
});
formatter.result({
  "duration": 3760016,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.newPlayer()"
});
formatter.result({
  "duration": 132416,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "9",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 191311,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "Jack",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 34259,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.startGame()"
});
formatter.result({
  "duration": 17322,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "7",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 33489,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.playHit()"
});
formatter.result({
  "duration": 17707,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.loseGame()"
});
formatter.result({
  "duration": 4089133,
  "status": "passed"
});
formatter.scenario({
  "id": "play-blackjack-game;house-over-21",
  "description": "",
  "name": "House over 21",
  "keyword": "Scenario",
  "line": 16,
  "type": "scenario"
});
formatter.step({
  "name": "a new deck card",
  "keyword": "Given ",
  "line": 17
});
formatter.step({
  "name": "a new player",
  "keyword": "Given ",
  "line": 18
});
formatter.step({
  "name": "next card is suit spades number 9",
  "keyword": "When ",
  "line": 19
});
formatter.step({
  "name": "next card is suit spades number Jack",
  "keyword": "When ",
  "line": 20
});
formatter.step({
  "name": "I start game",
  "keyword": "When ",
  "line": 21
});
formatter.step({
  "name": "I play stand",
  "keyword": "When ",
  "line": 22
});
formatter.step({
  "name": "house deals 22",
  "keyword": "When ",
  "line": 23
});
formatter.step({
  "name": "I win game",
  "keyword": "Then ",
  "line": 24
});
formatter.match({
  "location": "BlackjackStepsTest.newDeckCard()"
});
formatter.result({
  "duration": 210173,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.newPlayer()"
});
formatter.result({
  "duration": 78526,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "9",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 89304,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "Jack",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 77756,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.startGame()"
});
formatter.result({
  "duration": 36569,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.playStand()"
});
formatter.result({
  "duration": 46576,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "22",
      "offset": 12
    }
  ],
  "location": "BlackjackStepsTest.playStand(int)"
});
formatter.result({
  "duration": 428430,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.winGame()"
});
formatter.result({
  "duration": 75062,
  "status": "passed"
});
formatter.scenario({
  "id": "play-blackjack-game;dealer-better-than-house",
  "description": "",
  "name": "Dealer better than house",
  "keyword": "Scenario",
  "line": 26,
  "type": "scenario"
});
formatter.step({
  "name": "a new deck card",
  "keyword": "Given ",
  "line": 27
});
formatter.step({
  "name": "a new player",
  "keyword": "Given ",
  "line": 28
});
formatter.step({
  "name": "next card is suit spades number 9",
  "keyword": "When ",
  "line": 29
});
formatter.step({
  "name": "next card is suit spades number Jack",
  "keyword": "When ",
  "line": 30
});
formatter.step({
  "name": "I start game",
  "keyword": "When ",
  "line": 31
});
formatter.step({
  "name": "I play stand",
  "keyword": "When ",
  "line": 32
});
formatter.step({
  "name": "house deals 18",
  "keyword": "When ",
  "line": 33
});
formatter.step({
  "name": "I win game",
  "keyword": "Then ",
  "line": 34
});
formatter.match({
  "location": "BlackjackStepsTest.newDeckCard()"
});
formatter.result({
  "duration": 200550,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.newPlayer()"
});
formatter.result({
  "duration": 48501,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "9",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 83915,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "Jack",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 66978,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.startGame()"
});
formatter.result({
  "duration": 27330,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.playStand()"
});
formatter.result({
  "duration": 32334,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "18",
      "offset": 12
    }
  ],
  "location": "BlackjackStepsTest.playStand(int)"
});
formatter.result({
  "duration": 103932,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.winGame()"
});
formatter.result({
  "duration": 53121,
  "status": "passed"
});
formatter.scenario({
  "id": "play-blackjack-game;dealer-worse-than-house",
  "description": "",
  "name": "Dealer worse than house",
  "keyword": "Scenario",
  "line": 36,
  "type": "scenario"
});
formatter.step({
  "name": "a new deck card",
  "keyword": "Given ",
  "line": 37
});
formatter.step({
  "name": "a new player",
  "keyword": "Given ",
  "line": 38
});
formatter.step({
  "name": "next card is suit spades number 9",
  "keyword": "When ",
  "line": 39
});
formatter.step({
  "name": "next card is suit spades number Jack",
  "keyword": "When ",
  "line": 40
});
formatter.step({
  "name": "I start game",
  "keyword": "When ",
  "line": 41
});
formatter.step({
  "name": "I play stand",
  "keyword": "When ",
  "line": 42
});
formatter.step({
  "name": "house deals 20",
  "keyword": "When ",
  "line": 43
});
formatter.step({
  "name": "I lose game",
  "keyword": "Then ",
  "line": 44
});
formatter.match({
  "location": "BlackjackStepsTest.newDeckCard()"
});
formatter.result({
  "duration": 219411,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.newPlayer()"
});
formatter.result({
  "duration": 47347,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "9",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 74677,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "Jack",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 68903,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.startGame()"
});
formatter.result({
  "duration": 45037,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.playStand()"
});
formatter.result({
  "duration": 33874,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 12
    }
  ],
  "location": "BlackjackStepsTest.playStand(int)"
});
formatter.result({
  "duration": 107396,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.loseGame()"
});
formatter.result({
  "duration": 54275,
  "status": "passed"
});
formatter.uri('ort\practicasIS\laboratorio5\test\play_round.feature');
formatter.feature({
  "id": "play-options-blackjack",
  "description": "In order to play a round\r\nAs a user of the blackjack API\r\nI want to play a hit or stand",
  "name": "Play options blackjack",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "play-options-blackjack;play-a-hit",
  "description": "",
  "name": "Play a hit",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "a new deck card",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "a new player",
  "keyword": "Given ",
  "line": 8
});
formatter.step({
  "name": "next card is suit spades number 2",
  "keyword": "When ",
  "line": 9
});
formatter.step({
  "name": "next card is suit spades number 5",
  "keyword": "When ",
  "line": 10
});
formatter.step({
  "name": "I start game",
  "keyword": "When ",
  "line": 11
});
formatter.step({
  "name": "next card is suit spades number 7",
  "keyword": "When ",
  "line": 12
});
formatter.step({
  "name": "I play hit",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "I have card suit spades number 7",
  "keyword": "Then ",
  "line": 14
});
formatter.step({
  "name": "I have card suit spades number 2",
  "keyword": "Then ",
  "line": 15
});
formatter.step({
  "name": "I have card suit spades number 5",
  "keyword": "Then ",
  "line": 16
});
formatter.match({
  "location": "BlackjackStepsTest.newDeckCard()"
});
formatter.result({
  "duration": 4264277,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.newPlayer()"
});
formatter.result({
  "duration": 29255,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "2",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 160131,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "5",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 37723,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.startGame()"
});
formatter.result({
  "duration": 18091,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "7",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 59665,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.playHit()"
});
formatter.result({
  "duration": 25405,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 17
    },
    {
      "val": "7",
      "offset": 31
    }
  ],
  "location": "BlackjackStepsTest.hasCard(String,String)"
});
formatter.result({
  "duration": 82375,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 17
    },
    {
      "val": "2",
      "offset": 31
    }
  ],
  "location": "BlackjackStepsTest.hasCard(String,String)"
});
formatter.result({
  "duration": 34259,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 17
    },
    {
      "val": "5",
      "offset": 31
    }
  ],
  "location": "BlackjackStepsTest.hasCard(String,String)"
});
formatter.result({
  "duration": 32334,
  "status": "passed"
});
formatter.scenario({
  "id": "play-options-blackjack;play-a-stand",
  "description": "",
  "name": "Play a stand",
  "keyword": "Scenario",
  "line": 18,
  "type": "scenario"
});
formatter.step({
  "name": "a new deck card",
  "keyword": "Given ",
  "line": 19
});
formatter.step({
  "name": "a new player",
  "keyword": "Given ",
  "line": 20
});
formatter.step({
  "name": "next card is suit spades number 2",
  "keyword": "When ",
  "line": 21
});
formatter.step({
  "name": "next card is suit spades number 5",
  "keyword": "When ",
  "line": 22
});
formatter.step({
  "name": "I start game",
  "keyword": "When ",
  "line": 23
});
formatter.step({
  "name": "I play stand",
  "keyword": "When ",
  "line": 24
});
formatter.step({
  "name": "I have card suit spades number 2",
  "keyword": "Then ",
  "line": 25
});
formatter.step({
  "name": "I have card suit spades number 5",
  "keyword": "Then ",
  "line": 26
});
formatter.match({
  "location": "BlackjackStepsTest.newDeckCard()"
});
formatter.result({
  "duration": 214022,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.newPlayer()"
});
formatter.result({
  "duration": 25020,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "2",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 50426,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 18
    },
    {
      "val": "5",
      "offset": 32
    }
  ],
  "location": "BlackjackStepsTest.nextCard(String,String)"
});
formatter.result({
  "duration": 35414,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.startGame()"
});
formatter.result({
  "duration": 17322,
  "status": "passed"
});
formatter.match({
  "location": "BlackjackStepsTest.playStand()"
});
formatter.result({
  "duration": 16937,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 17
    },
    {
      "val": "2",
      "offset": 31
    }
  ],
  "location": "BlackjackStepsTest.hasCard(String,String)"
});
formatter.result({
  "duration": 33104,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spades",
      "offset": 17
    },
    {
      "val": "5",
      "offset": 31
    }
  ],
  "location": "BlackjackStepsTest.hasCard(String,String)"
});
formatter.result({
  "duration": 39263,
  "status": "passed"
});
});