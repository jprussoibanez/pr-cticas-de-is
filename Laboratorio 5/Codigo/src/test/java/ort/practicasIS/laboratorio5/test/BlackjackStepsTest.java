package ort.practicasIS.laboratorio5.test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import junit.framework.Assert;
import ort.practicasIS.laboratorio5.model.*;
import ort.practicasIS.laboratorio5.model.Player.GameStatus;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;

public class BlackjackStepsTest {

	private Deck cardDeck;
	private Player player;
	private List<Card> cards;
	private int houseDeal;
	public BlackjackStepsTest()
	{
	}
	
	@Given("^a new deck card$")  
	public void newDeckCard() {  
		this.cardDeck = new Deck();
	}  
	 
	@Given("^a new player$")  
	public void newPlayer() {  
		this.player = new Player(this.cardDeck);
	}  
	
	@When("^next card is suit ([a-zA-Z]*) number ([a-zA-Z0-9]*)$")  
	public void nextCard(String suit,String number) {  
		this.player.playNextCard(new Card(suit,number));
	}
	
	@When("^I start game$")  
	public void startGame() {  
	}

	@When("^I play hit$")  
	public void playHit() {  
	}

	@When("^I play stand$")  
	public void playStand() {  
	}

	@When("^house deals (\\d*)$")  
	public void playStand(int amountDeal) {
		houseDeal = amountDeal;
	}
	
	@When("^I get all cards in deck$")  
	public void getAllCardsInDeck() {  
		cards = cardDeck.getCards();
	}
	
	@When("^I play card suit ([a-zA-Z]*) number ([a-zA-Z0-9]*)$")
	public void playCard(String suit, String number) {  
		cardDeck.playNextCard(new Card(suit,number));
	}
	
	@Then("^I get card suit ([a-zA-Z]*) number ([a-zA-Z0-9]*)$")  
	public void getCard(String suit, String number) {  
		Assert.assertTrue(cards.contains(new Card(suit,number)));
	}
	
	@Then("^I have card suit ([a-zA-Z]*) number ([a-zA-Z0-9]*)$")  
	public void hasCard(String suit, String number) {  
		Assert.assertTrue(player.hasCard(new Card(suit,number)));
	}
	
	@Then("^card suit ([a-zA-Z]*) number ([a-zA-Z0-9]*) is not in deck$")
	public void cardNotInDeck(String suit, String number) {  
		Assert.assertFalse(cardDeck.isCardInDeck(new Card(suit,number)));
	}
	
	@Then("^I lose game$")
	public void loseGame() { 
		assertThat(player.finishGame(houseDeal),is(GameStatus.LOSE));
	}
	
	@Then("^I win game$")
	public void winGame() { 
		assertThat(player.finishGame(houseDeal),is(GameStatus.WIN));
	}
}