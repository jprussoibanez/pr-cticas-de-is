Feature: French deck cards
In order to play blackjack
As a user of the blackjack API
I want to use a french deck card to play

Scenario: Get all cards in deck
Given a new deck card
When I get all cards in deck
Then I get card suit spades number Ace
Then I get card suit spades number 2
Then I get card suit spades number Jack
Then I get card suit spades number Queen
Then I get card suit spades number King
Then I get card suit clubs number Ace
Then I get card suit diamonds number Ace
Then I get card suit hearts number Ace

Scenario: Take out card played
Given a new deck card
When I play card suit spades number Ace
Then card suit spades number Ace is not in deck