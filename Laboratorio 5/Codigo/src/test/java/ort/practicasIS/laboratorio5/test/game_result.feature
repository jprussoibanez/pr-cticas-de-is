Feature: Play blackjack game
In order to play blackjack
As a user of the blackjack API
I want to get the game result

Scenario: Dealer over 21
Given a new deck card
Given a new player
When next card is suit spades number 9
When next card is suit spades number Jack
When I start game
When next card is suit spades number 7
When I play hit
Then I lose game

Scenario: House over 21
Given a new deck card
Given a new player
When next card is suit spades number 9
When next card is suit spades number Jack
When I start game
When I play stand
When house deals 22
Then I win game

Scenario: Dealer better than house
Given a new deck card
Given a new player
When next card is suit spades number 9
When next card is suit spades number Jack
When I start game
When I play stand
When house deals 18
Then I win game

Scenario: Dealer worse than house
Given a new deck card
Given a new player
When next card is suit spades number 9
When next card is suit spades number Jack
When I start game
When I play stand
When house deals 20
Then I lose game