package ort.practicasIS.laboratorio5.model;
import java.util.*;

public class Card {
	private String suit;
	private String number;
	private static Map<String,Integer> map = new HashMap<String, Integer>(){{
		put("Ace", 1);
		put("2", 2);
	    put("3", 3);
	    put("4", 4);
	    put("5", 5);
	    put("6", 6);
	    put("7", 7);
	    put("8", 8);
	    put("9", 9);
	    put("Jack", 10);
	    put("Queen", 10);
	    put("King", 10);
	}};;
	
	public Card(String suit, String number)
	{
		this.suit = suit;
		this.number = number;
	}
		
	public String getSuit()
	{
		return suit;
	}
	
	public String getNumber()
	{
		return number;
	}
	
	public int getValue()
	{
		return map.get(number);
	}
	
	public boolean equals(Object obj)
	{
		Card card = (Card)obj;
		return this.suit.equals(card.getSuit()) && this.number.equals(card.getNumber());
	}
}