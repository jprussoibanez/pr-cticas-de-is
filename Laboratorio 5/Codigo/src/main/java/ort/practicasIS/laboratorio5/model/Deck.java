package ort.practicasIS.laboratorio5.model;

import java.util.*;
import java.util.regex.Pattern;

public class Deck {

	private List<Card> deckCards;
	private Card nextCard;
	
	public Deck()
	{
		initializeDeck();
	}
	
	private void initializeDeck()
	{
		deckCards = new ArrayList<Card>();
		deckCards.addAll(createDeckForSuit("spades"));
		deckCards.addAll(createDeckForSuit("hearts"));
		deckCards.addAll(createDeckForSuit("diamonds"));
		deckCards.addAll(createDeckForSuit("clubs"));	
	}
	
	private List<Card> createDeckForSuit(String suit)
	{
		List<Card>suitDeck = new ArrayList<Card>();
		suitDeck.add(new Card(suit,"Ace"));
		suitDeck.add(new Card(suit,"2"));
		suitDeck.add(new Card(suit,"3"));
		suitDeck.add(new Card(suit,"4"));
		suitDeck.add(new Card(suit,"5"));
		suitDeck.add(new Card(suit,"6"));
		suitDeck.add(new Card(suit,"7"));
		suitDeck.add(new Card(suit,"8"));
		suitDeck.add(new Card(suit,"9"));
		suitDeck.add(new Card(suit,"Jack"));
		suitDeck.add(new Card(suit,"Queen"));
		suitDeck.add(new Card(suit,"King"));
		
		return suitDeck;
	}
	
	public boolean isCardInDeck(Card card)
	{
		return deckCards.contains(card);
	}
	
	public Card playNextCard(Card card)
	{
		deckCards.remove(card);
		nextCard = card;
		return card;
	}
	
	public Card getNextCard()
	{
		return nextCard;
	}
	
	public List<Card> getCards()
	{
		return deckCards;
	}
}
