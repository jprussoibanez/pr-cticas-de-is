package ort.practicasIS.laboratorio5.model;

import java.util.*;

public class Player {
	private List<Card> cards;
	private Deck deck;
	public enum GameStatus {
		WIN,
		LOSE
	}
	public Player(Deck deck)
	{
		cards = new ArrayList<Card>();
		this.deck = deck;
	}
	
	public void addCard(Card card)
	{
		cards.add(card);
	}
	
	public boolean hasCard(Card card)
	{
		return cards.contains(card);
	}
	
	public int calculateValues()
	{
		int deckValue = 0;
		for (Card card : cards)
		{
			deckValue += card.getValue();
		}
		
		return deckValue;
	}
	
	public GameStatus finishGame(int houseDeal)
	{
		int playerDeal = calculateValues();
		if (playerDeal > 21)
			return GameStatus.LOSE;
		
		if (houseDeal > 21)
			return GameStatus.WIN;
		
		if (houseDeal < playerDeal)
			return GameStatus.WIN;
		
		return GameStatus.LOSE;
	}
	
	public void playNextCard(Card card)
	{
		addCard(deck.playNextCard(card));
	}
}