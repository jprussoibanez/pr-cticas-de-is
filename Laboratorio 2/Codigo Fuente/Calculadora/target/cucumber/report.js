$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri('ort\practicasIS\laboratorio2\calculadora\test\complex_operation.feature');
formatter.feature({
  "id": "multiple-operations",
  "description": "In order to calculate multiples operations\r\nAs a user of the calculator API\r\nI want to calculate multiples sum, multiple, subtract and divide operations",
  "name": "Multiple operations",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "multiple-operations;multiple-sum-calculation",
  "description": "",
  "name": "Multiple sum calculation",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "the inverse polac notation \u0027+ + 2 2 + 4 5\u0027",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I calculate the operation",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "I get result 13",
  "keyword": "Then ",
  "line": 9
});
formatter.match({
  "arguments": [
    {
      "val": "+ + 2 2 + 4 5",
      "offset": 28
    }
  ],
  "location": "CalculadoraStepsTest.polacNotation(String)"
});
formatter.result({
  "duration": 104495198,
  "status": "passed"
});
formatter.match({
  "location": "CalculadoraStepsTest.calculateOperation()"
});
formatter.result({
  "duration": 985811,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "13",
      "offset": 13
    }
  ],
  "location": "CalculadoraStepsTest.assertCalculatorResult(double)"
});
formatter.result({
  "duration": 2721469,
  "status": "passed"
});
formatter.scenario({
  "id": "multiple-operations;multiple-operations-calculation",
  "description": "",
  "name": "Multiple operations calculation",
  "keyword": "Scenario",
  "line": 11,
  "type": "scenario"
});
formatter.step({
  "name": "the inverse polac notation \u0027- + 2 2 / 8 * 1 2\u0027",
  "keyword": "Given ",
  "line": 12
});
formatter.step({
  "name": "I calculate the operation",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "I get result 0",
  "keyword": "Then ",
  "line": 14
});
formatter.match({
  "arguments": [
    {
      "val": "- + 2 2 / 8 * 1 2",
      "offset": 28
    }
  ],
  "location": "CalculadoraStepsTest.polacNotation(String)"
});
formatter.result({
  "duration": 82376,
  "status": "passed"
});
formatter.match({
  "location": "CalculadoraStepsTest.calculateOperation()"
});
formatter.result({
  "duration": 114710,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0",
      "offset": 13
    }
  ],
  "location": "CalculadoraStepsTest.assertCalculatorResult(double)"
});
formatter.result({
  "duration": 43112,
  "status": "passed"
});
formatter.scenario({
  "id": "multiple-operations;negative-result",
  "description": "",
  "name": "Negative result",
  "keyword": "Scenario",
  "line": 16,
  "type": "scenario"
});
formatter.step({
  "name": "the inverse polac notation \u0027- + 2 2 / 16 * 1 2\u0027",
  "keyword": "Given ",
  "line": 17
});
formatter.step({
  "name": "I calculate the operation",
  "keyword": "When ",
  "line": 18
});
formatter.step({
  "name": "I get result -4",
  "keyword": "Then ",
  "line": 19
});
formatter.match({
  "arguments": [
    {
      "val": "- + 2 2 / 16 * 1 2",
      "offset": 28
    }
  ],
  "location": "CalculadoraStepsTest.polacNotation(String)"
});
formatter.result({
  "duration": 93923,
  "status": "passed"
});
formatter.match({
  "location": "CalculadoraStepsTest.calculateOperation()"
});
formatter.result({
  "duration": 115095,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "-4",
      "offset": 13
    }
  ],
  "location": "CalculadoraStepsTest.assertCalculatorResult(double)"
});
formatter.result({
  "duration": 58894,
  "status": "passed"
});
formatter.uri('ort\practicasIS\laboratorio2\calculadora\test\invalid_operation.feature');
formatter.feature({
  "id": "invalid-operations",
  "description": "In order to handle invalid operations\r\nAs a user of the calculator API\r\nI want to be notify of errors",
  "name": "Invalid operations",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "invalid-operations;divide-by-0",
  "description": "",
  "name": "Divide by 0",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "the inverse polac notation \u0027/ 2 0\u0027",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I calculate the operation",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "I get infinity",
  "keyword": "Then ",
  "line": 9
});
formatter.match({
  "arguments": [
    {
      "val": "/ 2 0",
      "offset": 28
    }
  ],
  "location": "CalculadoraStepsTest.polacNotation(String)"
});
formatter.result({
  "duration": 2400051,
  "status": "passed"
});
formatter.match({
  "location": "CalculadoraStepsTest.calculateOperation()"
});
formatter.result({
  "duration": 71982,
  "status": "passed"
});
formatter.match({
  "location": "CalculadoraStepsTest.getInfinity()"
});
formatter.result({
  "duration": 1340334,
  "status": "passed"
});
formatter.uri('ort\practicasIS\laboratorio2\calculadora\test\simple_operation.feature');
formatter.feature({
  "id": "simple-operation",
  "description": "In order to calculate simple operations\r\nAs a user of the calculator API\r\nI want to calculate sum, multiple, subtract and divide operations",
  "name": "Simple operation",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "simple-operation;number-calculation",
  "description": "",
  "name": "Number calculation",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "the inverse polac notation \u00272\u0027",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I calculate the operation",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "I get result 2",
  "keyword": "Then ",
  "line": 9
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 28
    }
  ],
  "location": "CalculadoraStepsTest.polacNotation(String)"
});
formatter.result({
  "duration": 2479348,
  "status": "passed"
});
formatter.match({
  "location": "CalculadoraStepsTest.calculateOperation()"
});
formatter.result({
  "duration": 40033,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 13
    }
  ],
  "location": "CalculadoraStepsTest.assertCalculatorResult(double)"
});
formatter.result({
  "duration": 63129,
  "status": "passed"
});
formatter.scenario({
  "id": "simple-operation;sum-calculation",
  "description": "",
  "name": "Sum calculation",
  "keyword": "Scenario",
  "line": 11,
  "type": "scenario"
});
formatter.step({
  "name": "the inverse polac notation \u0027+ 2 2\u0027",
  "keyword": "Given ",
  "line": 12
});
formatter.step({
  "name": "I calculate the operation",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "I get result 4",
  "keyword": "Then ",
  "line": 14
});
formatter.match({
  "arguments": [
    {
      "val": "+ 2 2",
      "offset": 28
    }
  ],
  "location": "CalculadoraStepsTest.polacNotation(String)"
});
formatter.result({
  "duration": 80451,
  "status": "passed"
});
formatter.match({
  "location": "CalculadoraStepsTest.calculateOperation()"
});
formatter.result({
  "duration": 85840,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "4",
      "offset": 13
    }
  ],
  "location": "CalculadoraStepsTest.assertCalculatorResult(double)"
});
formatter.result({
  "duration": 50812,
  "status": "passed"
});
formatter.scenario({
  "id": "simple-operation;divide-calculation",
  "description": "",
  "name": "Divide calculation",
  "keyword": "Scenario",
  "line": 16,
  "type": "scenario"
});
formatter.step({
  "name": "the inverse polac notation \u0027/ 4 2\u0027",
  "keyword": "Given ",
  "line": 17
});
formatter.step({
  "name": "I calculate the operation",
  "keyword": "When ",
  "line": 18
});
formatter.step({
  "name": "I get result 2",
  "keyword": "Then ",
  "line": 19
});
formatter.match({
  "arguments": [
    {
      "val": "/ 4 2",
      "offset": 28
    }
  ],
  "location": "CalculadoraStepsTest.polacNotation(String)"
});
formatter.result({
  "duration": 105086,
  "status": "passed"
});
formatter.match({
  "location": "CalculadoraStepsTest.calculateOperation()"
});
formatter.result({
  "duration": 71213,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 13
    }
  ],
  "location": "CalculadoraStepsTest.assertCalculatorResult(double)"
});
formatter.result({
  "duration": 50041,
  "status": "passed"
});
formatter.scenario({
  "id": "simple-operation;multiple-calculation",
  "description": "",
  "name": "Multiple calculation",
  "keyword": "Scenario",
  "line": 21,
  "type": "scenario"
});
formatter.step({
  "name": "the inverse polac notation \u0027* 3 2\u0027",
  "keyword": "Given ",
  "line": 22
});
formatter.step({
  "name": "I calculate the operation",
  "keyword": "When ",
  "line": 23
});
formatter.step({
  "name": "I get result 6",
  "keyword": "Then ",
  "line": 24
});
formatter.match({
  "arguments": [
    {
      "val": "* 3 2",
      "offset": 28
    }
  ],
  "location": "CalculadoraStepsTest.polacNotation(String)"
});
formatter.result({
  "duration": 77757,
  "status": "passed"
});
formatter.match({
  "location": "CalculadoraStepsTest.calculateOperation()"
});
formatter.result({
  "duration": 66593,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "6",
      "offset": 13
    }
  ],
  "location": "CalculadoraStepsTest.assertCalculatorResult(double)"
});
formatter.result({
  "duration": 48117,
  "status": "passed"
});
formatter.scenario({
  "id": "simple-operation;subtract-calculation",
  "description": "",
  "name": "Subtract calculation",
  "keyword": "Scenario",
  "line": 26,
  "type": "scenario"
});
formatter.step({
  "name": "the inverse polac notation \u0027- 4 2\u0027",
  "keyword": "Given ",
  "line": 27
});
formatter.step({
  "name": "I calculate the operation",
  "keyword": "When ",
  "line": 28
});
formatter.step({
  "name": "I get result 2",
  "keyword": "Then ",
  "line": 29
});
formatter.match({
  "arguments": [
    {
      "val": "- 4 2",
      "offset": 28
    }
  ],
  "location": "CalculadoraStepsTest.polacNotation(String)"
});
formatter.result({
  "duration": 76601,
  "status": "passed"
});
formatter.match({
  "location": "CalculadoraStepsTest.calculateOperation()"
});
formatter.result({
  "duration": 3345829,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 13
    }
  ],
  "location": "CalculadoraStepsTest.assertCalculatorResult(double)"
});
formatter.result({
  "duration": 65438,
  "status": "passed"
});
});