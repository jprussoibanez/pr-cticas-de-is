Feature: Invalid operations
In order to handle invalid operations
As a user of the calculator API
I want to be notify of errors

Scenario: Divide by 0
Given the inverse polac notation '/ 2 0'
When I calculate the operation
Then I get infinity