Feature: Simple operation
In order to calculate simple operations
As a user of the calculator API
I want to calculate sum, multiple, subtract and divide operations

Scenario: Number calculation
Given the inverse polac notation '2'
When I calculate the operation
Then I get result 2

Scenario: Sum calculation
Given the inverse polac notation '+ 2 2'
When I calculate the operation
Then I get result 4

Scenario: Divide calculation
Given the inverse polac notation '/ 4 2'
When I calculate the operation
Then I get result 2

Scenario: Multiple calculation
Given the inverse polac notation '* 3 2'
When I calculate the operation
Then I get result 6

Scenario: Subtract calculation
Given the inverse polac notation '- 4 2'
When I calculate the operation
Then I get result 2