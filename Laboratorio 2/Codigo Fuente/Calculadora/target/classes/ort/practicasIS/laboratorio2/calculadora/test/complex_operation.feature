Feature: Multiple operations
In order to calculate multiples operations
As a user of the calculator API
I want to calculate multiples sum, multiple, subtract and divide operations

Scenario: Multiple sum calculation
Given the inverse polac notation '+ + 2 2 + 4 5'
When I calculate the operation
Then I get result 13

Scenario: Multiple operations calculation
Given the inverse polac notation '- + 2 2 / 8 * 1 2'
When I calculate the operation
Then I get result 0

Scenario: Negative result
Given the inverse polac notation '- + 2 2 / 16 * 1 2'
When I calculate the operation
Then I get result -4