package ort.practicasIS.laboratorio2.calculadora.test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import junit.framework.Assert;
import ort.practicasIS.laboratorio2.calculadora.model.*;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;

public class CalculadoraStepsTest {
	public String expression;
	public double calculatorResult;
	private String exceptionName;
	
	@Given("^the inverse polac notation '(.*)'$")  
	public void polacNotation(String expression) {  
		this.expression = expression;
	} 
	   
	@When("^I calculate the operation$")  
	public void calculateOperation() {  
		calculatorResult = Calculator.calculateRPN(this.expression);
	}    	
	
	@When("^I calculate the invalid operation$")  
	public void calculateInvalidOperation() {
		try {
			calculatorResult = Calculator.calculateRPN(this.expression);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			exceptionName = ex.getClass().getName();
		}
	}
	
	@Then("^I get result (-?\\d*)$")  
	public void assertCalculatorResult(double expectedResult) {
		assertThat(calculatorResult, is(expectedResult));  
	}
	
	@Then("^I get a ([a-zA-Z]*) exception$")  
	public void assertExceptionName(String expectedExceptionName) {
		assertThat(exceptionName, is(expectedExceptionName));  
	}
	
	@Then("^I get infinity$")  
	public void getInfinity() {  
		Assert.assertTrue(Double.isInfinite(calculatorResult));  
	}
}