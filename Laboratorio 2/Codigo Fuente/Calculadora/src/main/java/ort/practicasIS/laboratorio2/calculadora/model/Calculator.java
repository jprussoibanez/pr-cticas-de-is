package ort.practicasIS.laboratorio2.calculadora.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class Calculator {
	
	public static double calculateRPN(String expression)
	{
		List<String> tokens = Arrays.asList(expression.split(" "));
		Collections.reverse(tokens);
		Stack<String> tokensStack = new Stack<String>();
		tokensStack.addAll(tokens);
		
		return evaluateTokensStack(tokensStack);
	}
	
	public static double evaluateTokensStack(Stack<String> tokensStack)
	{
		String token = tokensStack.pop();
		double x,y;
		
		try {
			x = Double.parseDouble(token);
		} catch (Exception ex)
		{
			x = evaluateTokensStack(tokensStack);
			y = evaluateTokensStack(tokensStack);
			
			if ( token.equals("+") )
				x += y;
			if ( token.equals("-") )
				x -= y;
			if ( token.equals("*") )
				x *= y;
			if ( token.equals("/") )
				x /= y;
		}
		
		return x;
	}
}
