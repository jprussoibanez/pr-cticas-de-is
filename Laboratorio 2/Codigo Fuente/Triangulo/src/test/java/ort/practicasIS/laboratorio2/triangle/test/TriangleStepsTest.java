package ort.practicasIS.laboratorio2.triangle.test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import ort.practicasIS.laboratorio2.triangle.model.*;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;

public class TriangleStepsTest {
	private Triangle triangle;
	private Triangle.TriangleType triangleType;
	
	@Given("^three edges of a triangle (-?\\d*), (-?\\d*) and (-?\\d*)$")  
	public void threeEdgesTriangle(int firstEdge, int secondEdge, int thirdEdge) {  
		triangle = new Triangle(firstEdge, secondEdge, thirdEdge);
	} 
	   
	@When("^I calculate the triangle type$")  
	public void calculateTriangleType() {  
		triangleType = triangle.getTriangleType();
	}    	
	
	@Then("^I get an ([a-zA-Z]*) type$")  
	public void assertTriangleType(String triangleTypeText) {
		Triangle.TriangleType expectedTriangleType = Triangle.TriangleType.valueOf(triangleTypeText);
		assertThat(this.triangleType, is(expectedTriangleType));  
	}
}
