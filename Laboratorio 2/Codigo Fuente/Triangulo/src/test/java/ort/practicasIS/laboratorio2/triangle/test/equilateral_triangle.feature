Feature: Equilateral triangle
In order to recognize an equilateral triangle
As a user of the triangle API
I want to calculate equilateral triangles

Scenario: Positive equilateral
Given three edges of a triangle 3, 3 and 3
When I calculate the triangle type
Then I get an equilateral type