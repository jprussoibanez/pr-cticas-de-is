Feature: Not triangle
In order to recognize an not triangle
As a user of the triangle API
I want to calculate not triangles

Scenario: Negative first edge
Given three edges of a triangle -1, 3 and 3
When I calculate the triangle type
Then I get an notTriangle type

Scenario: Negative second edge
Given three edges of a triangle 3, -1 and 3
When I calculate the triangle type
Then I get an notTriangle type

Scenario: Negative third edge
Given three edges of a triangle 3, 3 and -1
When I calculate the triangle type
Then I get an notTriangle type

Scenario: Sum first and second less than third
Given three edges of a triangle 1, 2 and 4
When I calculate the triangle type
Then I get an notTriangle type

Scenario: Sum first and third less than second
Given three edges of a triangle 1, 4 and 2
When I calculate the triangle type
Then I get an notTriangle type

Scenario: Sum second and third less than first
Given three edges of a triangle 4, 1 and 2
When I calculate the triangle type
Then I get an notTriangle type