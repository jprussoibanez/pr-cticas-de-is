package ort.practicasIS.laboratorio2.triangle.model;

public class Triangle {

	public enum TriangleType {
		equilateral,
		isosceles,
		scalene,
		notTriangle
	}
	private int firstEdge;
	private int secondEdge;
	private int thirdEdge;
	
	public Triangle(int firstEdge, int secondEdge, int thirdEdge)
	{
		this.firstEdge = firstEdge;
		this.secondEdge = secondEdge;
		this.thirdEdge = thirdEdge;
	}
	
	public TriangleType getTriangleType()
	{
		if (!isTriangle())
			return TriangleType.notTriangle;
		if (isEquilateral())
			return TriangleType.equilateral;
		if (isIsosceles())
			return TriangleType.isosceles;
		if (isScalene())
			return TriangleType.scalene;
		
		return TriangleType.notTriangle;
	}
	
	private boolean isTriangle()
	{
		return hasPositiveEdges() && sumOfTwoEdgesGreaterThanThird();
	}
	
	private boolean sumOfTwoEdgesGreaterThanThird()
	{
		return firstEdge + secondEdge > thirdEdge &&
			   secondEdge + thirdEdge > firstEdge &&
			   firstEdge + thirdEdge > secondEdge;
	}
	
	private boolean hasPositiveEdges()
	{
		return firstEdge > 0 && secondEdge > 0 && thirdEdge > 0;
	}
	
	private boolean isEquilateral()
	{
		return firstEdge == secondEdge && secondEdge == thirdEdge;
	}
	
	private boolean isIsosceles()
	{
		return 	!isEquilateral() && (
				(firstEdge == secondEdge) ||
				(firstEdge == thirdEdge) ||
				(secondEdge == thirdEdge)
				);
	}
	
	private boolean isScalene()
	{
		return 	(firstEdge != secondEdge) &&
				(secondEdge != thirdEdge) &&
				(firstEdge != thirdEdge);
	}
}
