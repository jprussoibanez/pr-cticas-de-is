$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri('ort\practicasIS\laboratorio2\triangle\test\equilateral_triangle.feature');
formatter.feature({
  "id": "equilateral-triangle",
  "description": "In order to recognize an equilateral triangle\r\nAs a user of the triangle API\r\nI want to calculate equilateral triangles",
  "name": "Equilateral triangle",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "equilateral-triangle;positive-equilateral",
  "description": "",
  "name": "Positive equilateral",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "three edges of a triangle 3, 3 and 3",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I calculate the triangle type",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "I get an equilateral type",
  "keyword": "Then ",
  "line": 9
});
formatter.match({
  "arguments": [
    {
      "val": "3",
      "offset": 26
    },
    {
      "val": "3",
      "offset": 29
    },
    {
      "val": "3",
      "offset": 35
    }
  ],
  "location": "TriangleStepsTest.threeEdgesTriangle(int,int,int)"
});
formatter.result({
  "duration": 178556898,
  "status": "passed"
});
formatter.match({
  "location": "TriangleStepsTest.calculateTriangleType()"
});
formatter.result({
  "duration": 135111,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "equilateral",
      "offset": 9
    }
  ],
  "location": "TriangleStepsTest.assertTriangleType(String)"
});
formatter.result({
  "duration": 5249319,
  "status": "passed"
});
formatter.uri('ort\practicasIS\laboratorio2\triangle\test\isosceles_triangle.feature');
formatter.feature({
  "id": "isosceles-triangle",
  "description": "In order to recognize an isosceles triangle\r\nAs a user of the triangle API\r\nI want to calculate isosceles triangles",
  "name": "Isosceles triangle",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "isosceles-triangle;isosceles-first-and-second-edges",
  "description": "",
  "name": "Isosceles first and second edges",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "three edges of a triangle 5, 5 and 3",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I calculate the triangle type",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "I get an isosceles type",
  "keyword": "Then ",
  "line": 9
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 26
    },
    {
      "val": "5",
      "offset": 29
    },
    {
      "val": "3",
      "offset": 35
    }
  ],
  "location": "TriangleStepsTest.threeEdgesTriangle(int,int,int)"
});
formatter.result({
  "duration": 4017151,
  "status": "passed"
});
formatter.match({
  "location": "TriangleStepsTest.calculateTriangleType()"
});
formatter.result({
  "duration": 41572,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "isosceles",
      "offset": 9
    }
  ],
  "location": "TriangleStepsTest.assertTriangleType(String)"
});
formatter.result({
  "duration": 133956,
  "status": "passed"
});
formatter.scenario({
  "id": "isosceles-triangle;isosceles-first-and-third-edges",
  "description": "",
  "name": "Isosceles first and third edges",
  "keyword": "Scenario",
  "line": 11,
  "type": "scenario"
});
formatter.step({
  "name": "three edges of a triangle 5, 3 and 5",
  "keyword": "Given ",
  "line": 12
});
formatter.step({
  "name": "I calculate the triangle type",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "I get an isosceles type",
  "keyword": "Then ",
  "line": 14
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 26
    },
    {
      "val": "3",
      "offset": 29
    },
    {
      "val": "5",
      "offset": 35
    }
  ],
  "location": "TriangleStepsTest.threeEdgesTriangle(int,int,int)"
});
formatter.result({
  "duration": 220566,
  "status": "passed"
});
formatter.match({
  "location": "TriangleStepsTest.calculateTriangleType()"
});
formatter.result({
  "duration": 33104,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "isosceles",
      "offset": 9
    }
  ],
  "location": "TriangleStepsTest.assertTriangleType(String)"
});
formatter.result({
  "duration": 55430,
  "status": "passed"
});
formatter.scenario({
  "id": "isosceles-triangle;isosceles-second-and-third-edges",
  "description": "",
  "name": "Isosceles second and third edges",
  "keyword": "Scenario",
  "line": 16,
  "type": "scenario"
});
formatter.step({
  "name": "three edges of a triangle 3, 5 and 5",
  "keyword": "Given ",
  "line": 17
});
formatter.step({
  "name": "I calculate the triangle type",
  "keyword": "When ",
  "line": 18
});
formatter.step({
  "name": "I get an isosceles type",
  "keyword": "Then ",
  "line": 19
});
formatter.match({
  "arguments": [
    {
      "val": "3",
      "offset": 26
    },
    {
      "val": "5",
      "offset": 29
    },
    {
      "val": "5",
      "offset": 35
    }
  ],
  "location": "TriangleStepsTest.threeEdgesTriangle(int,int,int)"
});
formatter.result({
  "duration": 217102,
  "status": "passed"
});
formatter.match({
  "location": "TriangleStepsTest.calculateTriangleType()"
});
formatter.result({
  "duration": 38878,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "isosceles",
      "offset": 9
    }
  ],
  "location": "TriangleStepsTest.assertTriangleType(String)"
});
formatter.result({
  "duration": 65054,
  "status": "passed"
});
formatter.uri('ort\practicasIS\laboratorio2\triangle\test\not_triangle.feature');
formatter.feature({
  "id": "not-triangle",
  "description": "In order to recognize an not triangle\r\nAs a user of the triangle API\r\nI want to calculate not triangles",
  "name": "Not triangle",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "not-triangle;negative-first-edge",
  "description": "",
  "name": "Negative first edge",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "three edges of a triangle -1, 3 and 3",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I calculate the triangle type",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "I get an notTriangle type",
  "keyword": "Then ",
  "line": 9
});
formatter.match({
  "arguments": [
    {
      "val": "-1",
      "offset": 26
    },
    {
      "val": "3",
      "offset": 30
    },
    {
      "val": "3",
      "offset": 36
    }
  ],
  "location": "TriangleStepsTest.threeEdgesTriangle(int,int,int)"
});
formatter.result({
  "duration": 4402468,
  "status": "passed"
});
formatter.match({
  "location": "TriangleStepsTest.calculateTriangleType()"
});
formatter.result({
  "duration": 42342,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "notTriangle",
      "offset": 9
    }
  ],
  "location": "TriangleStepsTest.assertTriangleType(String)"
});
formatter.result({
  "duration": 200549,
  "status": "passed"
});
formatter.scenario({
  "id": "not-triangle;negative-second-edge",
  "description": "",
  "name": "Negative second edge",
  "keyword": "Scenario",
  "line": 11,
  "type": "scenario"
});
formatter.step({
  "name": "three edges of a triangle 3, -1 and 3",
  "keyword": "Given ",
  "line": 12
});
formatter.step({
  "name": "I calculate the triangle type",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "I get an notTriangle type",
  "keyword": "Then ",
  "line": 14
});
formatter.match({
  "arguments": [
    {
      "val": "3",
      "offset": 26
    },
    {
      "val": "-1",
      "offset": 29
    },
    {
      "val": "3",
      "offset": 36
    }
  ],
  "location": "TriangleStepsTest.threeEdgesTriangle(int,int,int)"
});
formatter.result({
  "duration": 198625,
  "status": "passed"
});
formatter.match({
  "location": "TriangleStepsTest.calculateTriangleType()"
});
formatter.result({
  "duration": 38108,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "notTriangle",
      "offset": 9
    }
  ],
  "location": "TriangleStepsTest.assertTriangleType(String)"
});
formatter.result({
  "duration": 66593,
  "status": "passed"
});
formatter.scenario({
  "id": "not-triangle;negative-third-edge",
  "description": "",
  "name": "Negative third edge",
  "keyword": "Scenario",
  "line": 16,
  "type": "scenario"
});
formatter.step({
  "name": "three edges of a triangle 3, 3 and -1",
  "keyword": "Given ",
  "line": 17
});
formatter.step({
  "name": "I calculate the triangle type",
  "keyword": "When ",
  "line": 18
});
formatter.step({
  "name": "I get an notTriangle type",
  "keyword": "Then ",
  "line": 19
});
formatter.match({
  "arguments": [
    {
      "val": "3",
      "offset": 26
    },
    {
      "val": "3",
      "offset": 29
    },
    {
      "val": "-1",
      "offset": 35
    }
  ],
  "location": "TriangleStepsTest.threeEdgesTriangle(int,int,int)"
});
formatter.result({
  "duration": 212098,
  "status": "passed"
});
formatter.match({
  "location": "TriangleStepsTest.calculateTriangleType()"
});
formatter.result({
  "duration": 29254,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "notTriangle",
      "offset": 9
    }
  ],
  "location": "TriangleStepsTest.assertTriangleType(String)"
});
formatter.result({
  "duration": 48117,
  "status": "passed"
});
formatter.scenario({
  "id": "not-triangle;sum-first-and-second-less-than-third",
  "description": "",
  "name": "Sum first and second less than third",
  "keyword": "Scenario",
  "line": 21,
  "type": "scenario"
});
formatter.step({
  "name": "three edges of a triangle 1, 2 and 4",
  "keyword": "Given ",
  "line": 22
});
formatter.step({
  "name": "I calculate the triangle type",
  "keyword": "When ",
  "line": 23
});
formatter.step({
  "name": "I get an notTriangle type",
  "keyword": "Then ",
  "line": 24
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 26
    },
    {
      "val": "2",
      "offset": 29
    },
    {
      "val": "4",
      "offset": 35
    }
  ],
  "location": "TriangleStepsTest.threeEdgesTriangle(int,int,int)"
});
formatter.result({
  "duration": 327962,
  "status": "passed"
});
formatter.match({
  "location": "TriangleStepsTest.calculateTriangleType()"
});
formatter.result({
  "duration": 46961,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "notTriangle",
      "offset": 9
    }
  ],
  "location": "TriangleStepsTest.assertTriangleType(String)"
});
formatter.result({
  "duration": 74292,
  "status": "passed"
});
formatter.scenario({
  "id": "not-triangle;sum-first-and-third-less-than-second",
  "description": "",
  "name": "Sum first and third less than second",
  "keyword": "Scenario",
  "line": 26,
  "type": "scenario"
});
formatter.step({
  "name": "three edges of a triangle 1, 4 and 2",
  "keyword": "Given ",
  "line": 27
});
formatter.step({
  "name": "I calculate the triangle type",
  "keyword": "When ",
  "line": 28
});
formatter.step({
  "name": "I get an notTriangle type",
  "keyword": "Then ",
  "line": 29
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 26
    },
    {
      "val": "4",
      "offset": 29
    },
    {
      "val": "2",
      "offset": 35
    }
  ],
  "location": "TriangleStepsTest.threeEdgesTriangle(int,int,int)"
});
formatter.result({
  "duration": 276381,
  "status": "passed"
});
formatter.match({
  "location": "TriangleStepsTest.calculateTriangleType()"
});
formatter.result({
  "duration": 43882,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "notTriangle",
      "offset": 9
    }
  ],
  "location": "TriangleStepsTest.assertTriangleType(String)"
});
formatter.result({
  "duration": 72367,
  "status": "passed"
});
formatter.scenario({
  "id": "not-triangle;sum-second-and-third-less-than-first",
  "description": "",
  "name": "Sum second and third less than first",
  "keyword": "Scenario",
  "line": 31,
  "type": "scenario"
});
formatter.step({
  "name": "three edges of a triangle 4, 1 and 2",
  "keyword": "Given ",
  "line": 32
});
formatter.step({
  "name": "I calculate the triangle type",
  "keyword": "When ",
  "line": 33
});
formatter.step({
  "name": "I get an notTriangle type",
  "keyword": "Then ",
  "line": 34
});
formatter.match({
  "arguments": [
    {
      "val": "4",
      "offset": 26
    },
    {
      "val": "1",
      "offset": 29
    },
    {
      "val": "2",
      "offset": 35
    }
  ],
  "location": "TriangleStepsTest.threeEdgesTriangle(int,int,int)"
});
formatter.result({
  "duration": 223260,
  "status": "passed"
});
formatter.match({
  "location": "TriangleStepsTest.calculateTriangleType()"
});
formatter.result({
  "duration": 30795,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "notTriangle",
      "offset": 9
    }
  ],
  "location": "TriangleStepsTest.assertTriangleType(String)"
});
formatter.result({
  "duration": 100468,
  "status": "passed"
});
formatter.uri('ort\practicasIS\laboratorio2\triangle\test\scalene_triangle.feature');
formatter.feature({
  "id": "scalene-triangle",
  "description": "In order to recognize an scalene triangle\r\nAs a user of the triangle API\r\nI want to calculate scalene triangles",
  "name": "Scalene triangle",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "scalene-triangle;positive-scalene",
  "description": "",
  "name": "Positive scalene",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "three edges of a triangle 3, 5 and 7",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I calculate the triangle type",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "I get an scalene type",
  "keyword": "Then ",
  "line": 9
});
formatter.match({
  "arguments": [
    {
      "val": "3",
      "offset": 26
    },
    {
      "val": "5",
      "offset": 29
    },
    {
      "val": "7",
      "offset": 35
    }
  ],
  "location": "TriangleStepsTest.threeEdgesTriangle(int,int,int)"
});
formatter.result({
  "duration": 5029137,
  "status": "passed"
});
formatter.match({
  "location": "TriangleStepsTest.calculateTriangleType()"
});
formatter.result({
  "duration": 48116,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "scalene",
      "offset": 9
    }
  ],
  "location": "TriangleStepsTest.assertTriangleType(String)"
});
formatter.result({
  "duration": 145889,
  "status": "passed"
});
});