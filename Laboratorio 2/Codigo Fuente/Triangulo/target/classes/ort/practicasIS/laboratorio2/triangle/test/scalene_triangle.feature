Feature: Scalene triangle
In order to recognize an scalene triangle
As a user of the triangle API
I want to calculate scalene triangles

Scenario: Positive scalene
Given three edges of a triangle 3, 5 and 7
When I calculate the triangle type
Then I get an scalene type