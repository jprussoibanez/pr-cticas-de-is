Feature: Isosceles triangle
In order to recognize an isosceles triangle
As a user of the triangle API
I want to calculate isosceles triangles

Scenario: Isosceles first and second edges
Given three edges of a triangle 5, 5 and 3
When I calculate the triangle type
Then I get an isosceles type

Scenario: Isosceles first and third edges
Given three edges of a triangle 5, 3 and 5
When I calculate the triangle type
Then I get an isosceles type

Scenario: Isosceles second and third edges
Given three edges of a triangle 3, 5 and 5
When I calculate the triangle type
Then I get an isosceles type