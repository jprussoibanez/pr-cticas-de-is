package ort.test;

import ort.domain.RoundScore;
import ort.persistence.ServerClient;
import ort.views.controllers.GameController;

public class FightScoreTest {
	
	private static GameController controller;
	
	public static void main(String[] args) {
		
		// Instancio el controlador de juego y seteo la pelea y juez
		controller = GameController.getInstance();
		controller.init();
		
		simulateRound();
		
		System.out.println(ServerClient.getInstance().hasAllJudgeCards());
		
	}
	
	public static void simulateRound(){
		
		controller.startRound();
		
		// El Boxer 1 dio 4 golpes
		controller.getActualRoundScore().addScoreToBoxer1();
		controller.getActualRoundScore().addScoreToBoxer1();
		controller.getActualRoundScore().addScoreToBoxer1();
		controller.getActualRoundScore().addScoreToBoxer1();
		
		// El Boxer 2 dio 2 golpes
		controller.getActualRoundScore().addScoreToBoxer2();
		controller.getActualRoundScore().addScoreToBoxer1();
		
		// El Boxer 2 tuvo un conteo
		controller.getActualRoundScore().addCountToBoxer2();
		
		controller.endRound();
	}
	
}
