/**
 * 
 */
package ort.test;

import static org.junit.Assert.*;

import org.junit.Test;

import ort.domain.Experience;
import ort.domain.FightType;

/**
 * @author Alvaro
 *
 */
public class FightTypeTest {

	@Test
	public void testFightType() {
		
		System.out.println("Test FightType");

		FightType ft1 = FightType.AMATEUR;
		FightType ft2 = FightType.CHAMPIONSHIP;
		
		assertTrue(ft1.compareTo(ft2)<0);
	}

}
