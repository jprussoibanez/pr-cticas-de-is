package ort.views;

import java.awt.EventQueue;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import ort.views.controllers.GameController;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import javax.swing.JRadioButton;

public class Cards extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	public JLayeredPane panelPlayer = new JLayeredPane();
	
	private static Cards instance = null;
	public GameController controller = GameController.getInstance();
	
	private Cards() {

		try {
			javax.swing.UIManager.setLookAndFeel(UIManager
					.getSystemLookAndFeelClassName());

//			Image im = Toolkit.getDefaultToolkit().getImage(
//					Img.class.getResource("network_transmit.png"));
//			this.setIconImage(im);

			this.setTitle("Judge cards");

			this.setResizable(false);
			initComponents();
			setLocationRelativeTo(null);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Cards getFrame() {
		if (instance == null) {
			instance = new Cards();
		}
		return instance;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cards frame = Cards.getFrame();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	private void initComponents() {
		
		controller.init();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 917, 571);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(114, 99, 646, 205);
		contentPane.add(panel);
		panel.setLayout(new GridLayout(2, 5));
		
		JLabel lblNewLabel = new JLabel("New label");
		panel.add(lblNewLabel, "2, 2");
		panel.add(lblNewLabel, "1, 1");
		lblNewLabel = new JLabel("Test");
		panel.add(lblNewLabel, "4, 4");
		panel.add(lblNewLabel, "4, 5");
		
										
	}
}
