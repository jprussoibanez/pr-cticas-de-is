package ort.views;

import javax.swing.JOptionPane;

public class UserMessage {
	
	private String message;
	
	private int messageType;
	
	public UserMessage(String message, int messageType) {
		super();
		this.message = message;
		this.messageType = messageType;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}
	
	public void display(){
		
		String message2Display = "Ha ocurrido un error inesperado :( ";
		
		if (message != null) {
			message2Display = message;
		}
		
		JOptionPane.showMessageDialog(null, message2Display, "System message",messageType);
	}

}
