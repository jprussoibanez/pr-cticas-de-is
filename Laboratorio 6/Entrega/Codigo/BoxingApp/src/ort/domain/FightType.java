package ort.domain;

public enum FightType {

	AMATEUR(4, "amatueur"),
	PROFESSIONAL(10, "profesional"),
	CHAMPIONSHIP(12, "de campeonato");
	
	private int rounds;
	private String lable;
	
	FightType(int rounds, String label) {
		this.rounds = rounds;
		this.lable = label;
	}
	
	public int getRounds() {
		return rounds;
	}

	public String getLable() {
		return lable;
	}
	
}
	
	

