package ort.domain;

import java.io.Serializable;

public class TableJudge extends Judge implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private JudgeCard card = new JudgeCard(this);
	
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TableJudge(int id) {
		super();
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "JUDGE ID: " + this.id;
	}

	public JudgeCard getCard() {
		return card;
	}
	
}