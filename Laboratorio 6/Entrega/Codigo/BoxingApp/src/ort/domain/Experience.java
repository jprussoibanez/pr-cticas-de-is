package ort.domain;

public enum Experience {

	CUATRO_ROUNDS(4),
	CINCO_ROUNDS(5),
	SEIS_ROUNDS(6),
	SIETE_ROUNDS(7),
	OCHO_ROUNDS(8),
	NUEVE_ROUNDS(9),
	DIEZ_ROUNDS(10),
	ONCE_ROUNDS(11),
	DOCE_ROUNDS(12),;
	
	private int rounds;
	
	Experience(int rounds) {
		this.rounds = rounds;
	}
	
	public int getRound() {
		return rounds;
	}
	
}
	
	

