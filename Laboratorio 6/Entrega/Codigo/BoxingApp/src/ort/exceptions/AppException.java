package ort.exceptions;

public class AppException extends Exception {
	
	private static final long serialVersionUID = 1L;

	private String message;
	private Exception exception;
	
	public AppException(String message, Exception exception) {
		super();
		this.message = message;
		this.exception = exception;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}
	
	
	
	

}
