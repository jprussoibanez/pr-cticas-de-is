package ort.persistence;

import com.hazelcast.core.Hazelcast;

public class BoxServer implements Runnable {

	public static void main(String[] args) {

		BoxServer server = new BoxServer();
		server.run();
		System.out.println("Servidor de jueces iniciado ... ");

	}

	@Override
	public void run() {

		// Inicializo el Map de Jueces
		Hazelcast.getMap("judgeMap");
		Hazelcast.getMap("fight");

	}

}
