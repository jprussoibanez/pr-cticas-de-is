package ort.utils;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

import ort.exceptions.AppException;

public class XMLUtils {

	public static Document readXML(String filePath) throws AppException {
		
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder;
			docBuilder = docBuilderFactory.newDocumentBuilder();
			
			return docBuilder.parse (new File(filePath));
			
		} catch (Exception e) {
			throw new AppException("Error al leer el archivo XML", e);
		}
		 
	}
	
}
