package ort.domain;

import java.io.Serializable;
import java.util.List;

public class Fight implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private List<Round> rounds;
	private int totalRounds;
	private Boxer boxer1;
	private Boxer boxer2;
	private FightType fightType;
	private int winnerBoxer;
	
	
	public Fight(FightType fightType, Boxer boxer1, Boxer boxer2) {
		
		this.boxer1 = boxer1;
		this.boxer2 = boxer2;
		this.fightType = fightType;

		// Calculo la cantidad de rounds
		this.totalRounds = calculateQtRounds();
			
	}
	
	public FightType getFightType() {
		return fightType;
	}



	public List<Round> getRounds() {
		return rounds;
	}

	public void setRounds(List<Round> rounds) {
		this.rounds = rounds;
	}
	
	public int getTotalRounds() {
		return totalRounds;
	}

	public void setTotalRounds(int totalRounds) {
		this.totalRounds = totalRounds;
	}

	public Boxer getBoxer1() {
		return boxer1;
	}

	public Boxer getBoxer2() {
		return boxer2;
	}

	/**
	 * Calcula la cantidad de rounds en base al tipo de pelea
	 * @return fight rounds
	 */
	private int calculateQtRounds(){
		return fightType.getRounds();
	}
	
	public int getWinnerBoxer() {
		return winnerBoxer;
	}

	public void setWinnerBoxer(int winnerBoxer) {
		this.winnerBoxer = winnerBoxer;
	}

	@Override
	public String toString() {
		return this.boxer1 + " ; " + this.boxer2 + " ; ROUNDS:" + this.getTotalRounds();
	}
	

}
