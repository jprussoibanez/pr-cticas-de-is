package ort.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que encapsula la los puntos de la pelea, por round, de un Juez
 * @author dmoretti
 *
 */
public class JudgeCard implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private TableJudge judge;
	private List<RoundScore> roundsScore = new ArrayList<RoundScore>();
	private int winnerBxerId = 0;
	
	public JudgeCard(TableJudge judge) {
		this.judge = judge;
	}
	
	public TableJudge getJudge() {
		return judge;
	}
	public void setJudge(TableJudge judge) {
		this.judge = judge;
	}
	public List<RoundScore> getRoundsScore() {
		return roundsScore;
	}
	public void addRoundScore(RoundScore roundScore){
		this.roundsScore.add(roundScore);
	}
	
	public int getWinnerBxerId() {
		return winnerBxerId;
	}

	public void setWinnerBxerId(int winnerBxerId) {
		this.winnerBxerId = winnerBxerId;
	}

	public RoundScore getRoundScore(int round){
		
		if (round <= roundsScore.size()) {
			return this.roundsScore.get(--round);
		}
		return null;
	}
	
	@Override
	public String toString() {
		
		String ds = "TARJETA DEL JUEZ " + judge.getId() + "\n";
		for (RoundScore roundScore : this.roundsScore) {
			ds += roundScore + "\n";
		}
		return ds;
	}
	
	public double getTotalPointBoxer(int boxerId){
		
		int points = 0;
		for (RoundScore roundScore : roundsScore) {
			if (boxerId == 1) {
				points += roundScore.getScoreBoxer1();
			}else{
				points += roundScore.getScoreBoxer2();
			}
		}
		return points;
		
	}
	
	public int getBoxerIdWinner(){
		
		if (winnerBxerId > 0) {
			return winnerBxerId;
		}else{
			double points1 = getTotalPointBoxer(1);
			double points2 = getTotalPointBoxer(2);
			
			if (points1 > points2) {
				return 1;
			} else if (points2 > points1){
				return 2;
			}
			return 0;
		}
		
		
	}
	
	
	
	
	
	
	
}
