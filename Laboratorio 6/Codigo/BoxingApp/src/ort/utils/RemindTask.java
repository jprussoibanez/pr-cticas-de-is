package ort.utils;

import java.util.TimerTask;

import ort.views.UI;
import ort.views.controllers.GameController;

public class RemindTask extends TimerTask {

	public void run() {
		UI form = UI.getFrame();
		form.seconds--;
		if (form.seconds == 0) {
			form.seconds = 60;
		}
		if (form.seconds == 59) {
			form.minutes--;
		}

		int secondsMOD60 = form.seconds % 60;
		String dsSeconds = (secondsMOD60 < 10) ? "0" + secondsMOD60 : ""
				+ secondsMOD60;

		form.clockMinutes.setText(String.valueOf(form.minutes));
		form.clockSeconds.setText(dsSeconds);

		if ((secondsMOD60 == 0 && form.minutes == 0) || GameController.getInstance().knockOut || GameController.getInstance().throwTowel) {
			form.timer.cancel();
			form.clockMinutes.setText("3");
			form.clockSeconds.setText("00");
			GameController.getInstance().endRound();
		}
	}
}
