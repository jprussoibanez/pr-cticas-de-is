package ort.utils;

import java.util.List;

import javax.swing.JOptionPane;

import ort.domain.JudgeCard;
import ort.persistence.ServerClient;
import ort.views.UserMessage;


public class WaitJudgeCardThread implements Runnable{
	
	@Override
	public void run() {
		
		while (!ServerClient.getInstance().hasAllJudgeCards()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}	
		}
		
		List<JudgeCard> judgesCards = ServerClient.getInstance().getJudgesCards();
		
		String punctuation = "";
		
		JudgeCard judgeCard1 = judgesCards.get(0);
		int winner1Id = judgeCard1.getBoxerIdWinner();

		JudgeCard judgeCard2 = judgesCards.get(1);
		int winner2Id = judgeCard2.getBoxerIdWinner();
		
		JudgeCard judgeCard3 = judgesCards.get(2);
		int winner3Id = judgeCard3.getBoxerIdWinner();
		
		String finalResult = "";
		
		
		if (winner1Id == 1) {
			
			if (winner2Id == 1) {
				finalResult = "GANO EL BOXEADOR 1";
			}else if (winner2Id == 0) {
				if (winner3Id == 0 || winner3Id == 1) {
					finalResult = "GANO EL BOXEADOR 1";
				}else if (winner3Id == 2){
					finalResult = "EMPATE";
				}
			}else if (winner2Id == 2) {
				
				if (winner3Id == 1) {
					finalResult = "GANO EL BOXEADOR 1";
				}else if (winner3Id == 2){
					finalResult = "GANO EL BOXEADOR 2";
				}else{
					finalResult = "EMPATE";
				}
				
			}
		
		} else if (winner1Id == 2) {
			
			if (winner2Id == 2) {
				finalResult = "GANO EL BOXEADOR 2";
			}else if (winner2Id == 0) {
				if (winner3Id == 0 || winner3Id == 2) {
					finalResult = "GANO EL BOXEADOR 2";
				}else if (winner3Id == 1){
					finalResult = "EMPATE";
				}
			}else if (winner2Id == 1) {
				
				if (winner3Id == 1) {
					finalResult = "GANO EL BOXEADOR 1";
				}else if (winner3Id == 2){
					finalResult = "GANO EL BOXEADOR 2";
				}else{
					finalResult = "EMPATE";
				}
				
			}
		}  else if (winner1Id == 0) {
			
			if (winner2Id == 0) {
				finalResult = "EMPATE";
			}else if (winner2Id == 1) {
				if (winner3Id == 0 || winner3Id == 1) {
					finalResult = "GANO EL BOXEADOR 1";
				}else if (winner3Id == 2){
					finalResult = "EMPATE";
				}
			}else if (winner2Id == 2) {
				
				if (winner3Id == 1) {
					finalResult = "EMPATE";
				}else if (winner3Id == 2){
					finalResult = "GANO EL BOXEADOR 2";
				}else{
					finalResult = "GANO EL BOXEADOR 2";
				}
				
			}
		}
		
		
		for (JudgeCard judgeCard : judgesCards) {
			
			int winnerId = judgeCard.getBoxerIdWinner();
			
			String result = "EMPATE";
			if (winnerId == 1) {
				result = "GANO EL BOXEADOR 1 !";
			}else if(winnerId == 2){
				result = "GANO EL BOXEADOR 2 !";
			}
			punctuation += "JUEZ " + judgeCard.getJudge().getId() + ": " + result + "\n";
		}
		punctuation += "-------------------------------------------\n";
		punctuation += "RESULTADO FINAL:  " + finalResult + "\n";
		 
		UserMessage message = new UserMessage(punctuation,JOptionPane.INFORMATION_MESSAGE);
		message.display();
		
		
	}
	

}
