package ort.views;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import ort.views.controllers.FormActionController;
import ort.views.controllers.GameController;
import ort.views.img.Img;

public class UI extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	public JLayeredPane panelPlayer = new JLayeredPane();
	public Timer timer;
	public boolean enableRoundButtons = false;

	private static UI instance = null;
	public GameController controller = GameController.getInstance();

	public JLabel clockMinutes;
	public JLabel clockSeconds;
	public int seconds = 60;
	public int minutes = 3;
	private JLabel label;
	private JLabel imgBoxer1;
	private JLabel lblNameBoxer1;
	public JButton btnBox1Kick;
	public JButton btnBox1Count;
	public JButton btnWinBox1KO;
	public JButton btnWinBox1TT;
	public JLabel lblPositivePointsBoxer1;
	public JLabel lblNegativePointsBoxer1;
	private JPanel panel_1;
	private JLabel imgBoxer2;
	private JLabel lblNameBoxer2;
	public JButton btnBox2Count;
	public JButton btnBox2Kick;
	public JButton btnWinBox2KO;
	public JButton btnWinBox2TT;
	public  JLabel lblPositivePointsBoxer2;
	public  JLabel lblNegativePointsBoxer2;
	public JButton btnNewRound;
	private JPanel panel_2;
	private JLabel lblNewLabel;
	private JLabel label_1;
	private JLabel label_2;
	private JLabel label_3;
	public JLabel lblRound;
	public JLabel message;
	public JLabel lblPelea;
	public JLabel label_4;
	public JLabel label_5;

	private UI() {

		// Set layout
		try {
			javax.swing.UIManager.setLookAndFeel(UIManager
					.getSystemLookAndFeelClassName());

			// Image im = Toolkit.getDefaultToolkit().getImage(
			// Img.class.getResource("network_transmit.png"));
			// this.setIconImage(im);

			this.setTitle("Box");

			this.setResizable(false);
			initComponents();
			setLocationRelativeTo(null);

			controller.init();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static UI getFrame() {
		if (instance == null) {
			instance = new UI();
		}
		return instance;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UI frame = UI.getFrame();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	private void initComponents() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 854, 571);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel timerPanel = new JPanel();
		timerPanel.setLayout(null);
		timerPanel.setBounds(0, 11, 848, 71);

		contentPane.add(timerPanel);

		clockSeconds = new JLabel("",JLabel.CENTER);
		clockSeconds.setBounds(734, 0, 104, 46);
		clockSeconds.setBorder(BorderFactory.createRaisedBevelBorder());
		clockSeconds.setFont(new Font("Serif", Font.BOLD, 38));
		
		clockMinutes = new JLabel("",JLabel.CENTER);
		clockMinutes.setBounds(651, 0, 57, 46);
		clockMinutes.setBorder(BorderFactory.createRaisedBevelBorder());
		clockMinutes.setFont(new Font("Serif", Font.BOLD, 38));
		//lblNewLabel.setForeground(Color.white);

		timerPanel.add(clockMinutes);
		timerPanel.add(clockSeconds);
		
		label = new JLabel(":",JLabel.CENTER);
		label.setBounds(709, 0, 26, 46);
		label.setFont(new Font("Serif", Font.BOLD, 38));
		timerPanel.add(label);

		JSeparator separator = new JSeparator();
		separator.setBounds(0, 93, 960, 2);
		contentPane.add(separator);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(BorderFactory.createRaisedBevelBorder());
		panel.setBounds(10, 106, 401, 344);
		contentPane.add(panel);
		
		imgBoxer1 = new JLabel("", JLabel.CENTER);
		imgBoxer1.setIcon(new ImageIcon(Img.class.getResource("boxer1.png")));
		imgBoxer1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		imgBoxer1.setBounds(134, 28, 130, 129);
		panel.add(imgBoxer1);
		
		lblNameBoxer1 = new JLabel("Boxeador 1", JLabel.CENTER);
		lblNameBoxer1.setBounds(36, 168, 328, 42);
		panel.add(lblNameBoxer1);
		
		btnBox1Count = new JButton(" Recibe conteo ");
		btnBox1Count.setEnabled(enableRoundButtons);
		btnBox1Count.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				FormActionController.addCountBoxer(1);
			}
		});
		btnBox1Count.setBounds(205, 224, 159, 49);
		panel.add(btnBox1Count);
		
		btnBox1Kick = new JButton(" Golpea al rival ");
		btnBox1Kick.setEnabled(enableRoundButtons);
		btnBox1Kick.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FormActionController.addScoreBoxer(1);
			}
		});
		btnBox1Kick.setBounds(36, 224, 159, 49);
		panel.add(btnBox1Kick);
		
		btnWinBox1KO = new JButton(" Gana por Knock out ");
		btnWinBox1KO.setEnabled(enableRoundButtons);
		btnWinBox1KO.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FormActionController.knokout(1);
			}
		});
		btnWinBox1KO.setBounds(36, 284, 159, 49);
		panel.add(btnWinBox1KO);
		
		btnWinBox1TT = new JButton(" Rival tira la toalla");
		btnWinBox1TT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FormActionController.throwTowel(1);
			}
		});
		btnWinBox1TT.setEnabled(enableRoundButtons);
		btnWinBox1TT.setBounds(205, 284, 159, 49);
		panel.add(btnWinBox1TT);
		
		lblPositivePointsBoxer1 = new JLabel("0", JLabel.CENTER);
		lblPositivePointsBoxer1.setBounds(36, 65, 68, 59);
		lblPositivePointsBoxer1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		lblPositivePointsBoxer1.setFont(new Font("Serif", Font.BOLD, 30));
		lblPositivePointsBoxer1.setOpaque(true);
		lblPositivePointsBoxer1.setBackground(Color.WHITE);
		panel.add(lblPositivePointsBoxer1);
		
		lblNegativePointsBoxer1 = new JLabel("0", JLabel.CENTER);
		lblNegativePointsBoxer1.setBounds(296, 65, 68, 59);
		lblNegativePointsBoxer1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		lblNegativePointsBoxer1.setFont(new Font("Serif", Font.BOLD, 30));
		lblNegativePointsBoxer1.setOpaque(true);
		lblNegativePointsBoxer1.setBackground(Color.WHITE);
		panel.add(lblNegativePointsBoxer1);
		
		lblNewLabel = new JLabel("Golpes",JLabel.CENTER);
		lblNewLabel.setBounds(36, 135, 68, 14);
		panel.add(lblNewLabel);
		
		label_2 = new JLabel("Conteos", SwingConstants.CENTER);
		label_2.setBounds(296, 135, 68, 14);
		panel.add(label_2);
		
		panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBorder(BorderFactory.createRaisedBevelBorder());
		panel_1.setBounds(435, 106, 401, 344);
		contentPane.add(panel_1);
		
		imgBoxer2 = new JLabel("", SwingConstants.CENTER);
		imgBoxer2.setBackground(Color.WHITE);
		imgBoxer2.setIcon(new ImageIcon(Img.class.getResource("boxer2.png")));
		imgBoxer2.setBounds(135, 28, 130, 129);
		imgBoxer2.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		panel_1.add(imgBoxer2);
		
		lblNameBoxer2 = new JLabel("Boxeador 2", SwingConstants.CENTER);
		lblNameBoxer2.setBounds(36, 168, 328, 42);
		panel_1.add(lblNameBoxer2);
		
		btnBox2Count = new JButton(" Recibe conteo ");
		btnBox2Count.setEnabled(enableRoundButtons);
		btnBox2Count.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FormActionController.addCountBoxer(2);
			}
		});
		btnBox2Count.setBounds(205, 224, 159, 49);
		panel_1.add(btnBox2Count);
		
		btnBox2Kick = new JButton(" Golpea al rival ");
		btnBox2Kick.setEnabled(enableRoundButtons);
		btnBox2Kick.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FormActionController.addScoreBoxer(2);
			}
		});
		btnBox2Kick.setBounds(36, 224, 159, 49);
		panel_1.add(btnBox2Kick);
		
		btnWinBox2KO = new JButton(" Gana por Knock out ");
		btnWinBox2KO.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FormActionController.throwTowel(2);
			}
		});
		btnWinBox2KO.setEnabled(enableRoundButtons);
		btnWinBox2KO.setBounds(36, 284, 159, 49);
		panel_1.add(btnWinBox2KO);
		
		btnWinBox2TT = new JButton(" Rival tira la toalla");
		btnWinBox2TT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FormActionController.throwTowel(2);
			}
		});
		btnWinBox2TT.setEnabled(enableRoundButtons);
		btnWinBox2TT.setBounds(205, 284, 159, 49);
		panel_1.add(btnWinBox2TT);
		
		lblPositivePointsBoxer2 = new JLabel("0", JLabel.CENTER);
		lblPositivePointsBoxer2.setFont(new Font("Serif", Font.BOLD, 30));
		lblPositivePointsBoxer2.setBounds(36, 65, 68, 59);
		lblPositivePointsBoxer2.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		lblPositivePointsBoxer2.setFont(new Font("Serif", Font.BOLD, 30));
		lblPositivePointsBoxer2.setOpaque(true);
		lblPositivePointsBoxer2.setBackground(Color.WHITE);
		
		panel_1.add(lblPositivePointsBoxer2);
		
		
		lblNegativePointsBoxer2 = new JLabel("0", JLabel.CENTER);
		lblNegativePointsBoxer2.setBounds(296, 65, 68, 59);
		lblNegativePointsBoxer2.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		lblNegativePointsBoxer2.setFont(new Font("Serif", Font.BOLD, 30));
		lblNegativePointsBoxer2.setOpaque(true);
		lblNegativePointsBoxer2.setBackground(Color.WHITE);
		panel_1.add(lblNegativePointsBoxer2);
		
		label_1 = new JLabel("Golpes", SwingConstants.CENTER);
		label_1.setBounds(36, 135, 68, 14);
		panel_1.add(label_1);
		
		label_3 = new JLabel("Conteos", SwingConstants.CENTER);
		label_3.setBounds(296, 135, 68, 14);
		panel_1.add(label_3);
		
		panel_2 = new JPanel();
		panel_2 .setLayout(null);
		panel_2.setBorder(BorderFactory.createRaisedBevelBorder());
		panel_2.setBounds(10, 461, 826, 74);
		contentPane.add(panel_2);
		
		btnNewRound = new JButton("Comienza nuevo round");
		btnNewRound.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FormActionController.startNewRound();
			}
		});
		btnNewRound.setBounds(276, 15, 286, 41);
		panel_2.add(btnNewRound);
		
		clockMinutes.setText("3");
		clockSeconds.setText("00");
		
		lblRound = new JLabel("", SwingConstants.CENTER);
		lblRound.setFont(new Font("Serif", Font.BOLD, 26));
		lblRound.setBounds(270, 0, 226, 46);
		timerPanel.add(lblRound);
		
		message = new JLabel("Esperando por los resultados de los otros jueces ...");
		message.setVisible(false);
		message.setBounds(292, 57, 290, 14);
		timerPanel.add(message);
		
		lblPelea = new JLabel("", SwingConstants.CENTER);
		lblPelea.setFont(new Font("Serif", Font.BOLD, 24));
		lblPelea.setBounds(22, 0, 226, 46);
		timerPanel.add(lblPelea);
		
		label_4 = new JLabel("", SwingConstants.CENTER);
		label_4.setFont(new Font("Serif", Font.BOLD, 26));
		label_4.setBounds(538, 0, 44, 46);
		timerPanel.add(label_4);
		
		label_5 = new JLabel("", SwingConstants.CENTER);
		label_5.setFont(new Font("Serif", Font.BOLD, 26));
		label_5.setBounds(506, 0, 19, 46);
		timerPanel.add(label_5);

	}
	
	public void resetTimer(){
		seconds = 60;
		minutes = 3;
		
	}

	
}
