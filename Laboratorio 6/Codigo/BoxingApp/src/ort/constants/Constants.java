package ort.constants;

public class Constants {
	
	public static double WINNER_ROUND_POINTS = 10;
	public static double LOSER_ROUND_POINTS = 9;
	public static double LITTLE_WINNER_ROUND_POINTS = 9.5;
	public static double DRAW_ROUND_POINTS = 9;
	
	public static int SECONDS_TIME_UNIT = 100;
	
}
