/**
 * 
 */
package ort.test;

import static org.junit.Assert.*;

import org.junit.Test;

import ort.domain.Boxer;
import ort.domain.Experience;

/**
 * @author Alvaro
 *
 */
public class ExperienceTest {

	@Test
	public void testExperiencia() {
		System.out.println("Test Experiencia");
		Experience exp1 = Experience.CUATRO_ROUNDS;
		Experience exp2 = Experience.CINCO_ROUNDS;
		assertTrue(exp1.compareTo(exp2)<0);
	}
	
	@Test
	public void testExperienciaValor4() {
		System.out.println("Test Experiencia");
		Experience exp1 = Experience.CUATRO_ROUNDS;
		assertTrue(exp1.getRound()==4);
	}
	
	@Test
	public void testExperienciaValor5() {
		System.out.println("Test Experiencia");
		Experience exp1 = Experience.CINCO_ROUNDS;
		assertTrue(exp1.getRound()==5);
		
	}
	
	@Test
	public void testExperienciaValor6() {
		System.out.println("Test Experiencia");
		Experience exp1 = Experience.SEIS_ROUNDS;
		assertTrue(exp1.getRound()==6);
	}
	
	@Test
	public void testExperienciaValor7() {
		System.out.println("Test Experiencia");
		Experience exp1 = Experience.SIETE_ROUNDS;
		assertTrue(exp1.getRound()==7);
	}
	
	@Test
	public void testExperienciaValor8() {
		System.out.println("Test Experiencia");
		Experience exp1 = Experience.OCHO_ROUNDS;
		assertTrue(exp1.getRound()==8);
	}
	
	@Test
	public void testExperienciaValor9() {
		System.out.println("Test Experiencia");
		Experience exp1 = Experience.NUEVE_ROUNDS;
		assertTrue(exp1.getRound()==9);
	}
	
	@Test
	public void testExperienciaValor10() {
		System.out.println("Test Experiencia");
		Experience exp1 = Experience.DIEZ_ROUNDS;
		assertTrue(exp1.getRound()==10);
	}
	
	@Test
	public void testExperienciaValor11() {
		System.out.println("Test Experiencia");
		Experience exp1 = Experience.ONCE_ROUNDS;
		assertTrue(exp1.getRound()==11);
	}
	
	@Test
	public void testExperienciaValor12() {
		System.out.println("Test Experiencia");
		Experience exp1 = Experience.DOCE_ROUNDS;
		assertTrue(exp1.getRound()==12);
	}


	

}
