/**
 * 
 */
package ort.test;

import static org.junit.Assert.*;

import org.junit.Test;

import ort.domain.Boxer;
import ort.domain.Experience;

/**
 * @author Alvaro
 *
 */
public class BoxerTest {

	@Test
	public void testNotNull() {
		
		System.out.println("Test Not Null");
		
		Experience exp1 = Experience.CUATRO_ROUNDS;
		String nom1 = new String("Juan");
		Boxer b1 = new Boxer(nom1,exp1);
		
		assertNotNull (nom1, b1); 
	}
	
	@Test
	public void testObjetosDiferentes() {
		
		System.out.println("Test Objetos Diferentes");
		
		Experience exp1 = Experience.CUATRO_ROUNDS;
		String nom1 = new String("Juan");
		Boxer b1 = new Boxer(nom1,exp1);
		
		Experience exp2 = Experience.CINCO_ROUNDS;
		String nom2 = new String("Juan");
		Boxer b2 = new Boxer(nom2,exp2);
		
		assertNotSame (b1, b2);
	}
	
	@Test
	public void testExperiencia() {
		
		System.out.println("Test Experiencia");
		
		Experience exp1 = Experience.CUATRO_ROUNDS;
		String nom1 = new String("Juan");
		Boxer b1 = new Boxer(nom1,exp1);
		
		Experience exp2 = Experience.CINCO_ROUNDS;
		String nom2 = new String("Juan");
		Boxer b2 = new Boxer(nom2,exp2);
		
		assertTrue(b1.getExperience().compareTo(b2.getExperience())<0);
	}

}
