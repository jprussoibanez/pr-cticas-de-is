/**
 * 
 */
package ort.test;

import static org.junit.Assert.*;

import org.junit.Test;

import ort.domain.JudgeCard;
import ort.domain.TableJudge;

/**
 * @author varo
 *
 */
public class TableJudgeTest {

	@Test
	public void testIsNull() {
		
		System.out.println("Test TableJudge isNull");
		TableJudge j1 = null;
		assertNull (j1);
	}
	
	@Test
	public void teNotNull() {
		
		System.out.println("Test TableJudge isNull");
		TableJudge j1 = new TableJudge(2);
		assertNotNull (j1);
	}
	
	@Test
	public void testDiferentes() {
		
		System.out.println("Test TableJudge Diferentes");
		TableJudge j1 = new TableJudge(5);
		TableJudge j2 = new TableJudge(3);
		assertNotSame(j1, j2);
	}
	
	@Test
	public void testCambioValor() {
		
		System.out.println("Test TableJudge Diferentes");
		TableJudge j1 = new TableJudge(5);
		j1.setId(12);
		assertTrue(j1.getId()==12);
	}

}
