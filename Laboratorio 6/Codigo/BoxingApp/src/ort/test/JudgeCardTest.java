/**
 * 
 */
package ort.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import ort.domain.Judge;
import ort.domain.JudgeCard;
import ort.domain.RoundScore;
import ort.domain.TableJudge;
import ort.views.controllers.GameController;

/**
 * @author varo
 *
 */
public class JudgeCardTest {

	@Test
	public void testDiferentes() {
		
		System.out.println("Test JudgeCard Diferentes");
		TableJudge j1 = null;
		JudgeCard jc1 = new JudgeCard(j1);
		TableJudge j2 = null;
		JudgeCard jc2 = new JudgeCard(j2);
		assertNotSame(jc1, jc2);
	}
	
	@Test
	public void testNotNull() {
		
		System.out.println("Test JudgeCard NotNull");
		TableJudge j1 = null;
		JudgeCard jc1 = new JudgeCard(j1);
		assertNotNull(jc1);
	}
	
	@Test
	public void testDiferentesRoundScores() {
		
		System.out.println("Test JudgeCard Diferentes");
		
		TableJudge j1 = null;
		JudgeCard jc1 = new JudgeCard(j1);
		RoundScore rs1 = new RoundScore();
		jc1.addRoundScore(rs1);
		
		TableJudge j2 = null;
		JudgeCard jc2 = new JudgeCard(j2);
		RoundScore rs2 = new RoundScore();
		rs2.addScoreToBoxer1();
		rs2.addScoreToBoxer1();
		rs2.addScoreToBoxer1();
		rs2.addScoreToBoxer1();
		rs2.addScoreToBoxer2();
		rs2.addScoreToBoxer2();
		rs2.addScoreToBoxer2();
		rs2.closeRoundScore();
		jc1.addRoundScore(rs2);

		jc2.addRoundScore(rs2);
		
		assertNotSame(jc1.getRoundsScore(),jc2.getRoundsScore());
	}
	
	@Test
	public void testRoundScores() {
		
		System.out.println("Test RoundScores");
		
		TableJudge j1 = null;
		JudgeCard jc1 = new JudgeCard(j1);
		RoundScore rs1 = new RoundScore();
		jc1.addRoundScore(rs1);
		
		TableJudge j2 = null;
		JudgeCard jc2 = new JudgeCard(j2);
		RoundScore rs2 = new RoundScore();
		rs2.addScoreToBoxer1();
		rs2.addScoreToBoxer1();
		rs2.addScoreToBoxer1();
		rs2.addScoreToBoxer1();
		rs2.addScoreToBoxer2();
		rs2.addScoreToBoxer2();
		rs2.addScoreToBoxer2();
		rs2.closeRoundScore();
		jc1.addRoundScore(rs2);

		jc2.addRoundScore(rs2);
		
		assertNotSame(jc1.getRoundsScore(),jc2.getRoundsScore());
	}
	
	@Test
	public void testSameJudges() {
		
		System.out.println("Test Same Judges");
		TableJudge j1 = null;
		JudgeCard jc1 = new JudgeCard(j1);
		JudgeCard jc2 = new JudgeCard(j1);
		assertEquals(jc1.getJudge(),jc2.getJudge());
	}

}
