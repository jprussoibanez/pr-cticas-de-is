package ort.test;

import ort.domain.RoundScore;
import ort.views.controllers.GameController;

public class RoundScoreTest {
	
	private static GameController controller;
	
	public static void main(String[] args) {
		
		// Instancio el controlador de juego y seteo la pelea y juez
		controller = GameController.getInstance();
		controller.init();
		
		testRoundWinBox1();
		testRoundDraw();
		
	}
	
	public static void testRoundWinBox1(){
		
		controller.startRound();
		
		// El Boxer 1 dio 4 golpes
		controller.getActualRoundScore().addScoreToBoxer1();
		controller.getActualRoundScore().addScoreToBoxer1();
		controller.getActualRoundScore().addScoreToBoxer1();
		controller.getActualRoundScore().addScoreToBoxer1();
		
		// El Boxer 2 dio 2 golpes
		controller.getActualRoundScore().addScoreToBoxer2();
		controller.getActualRoundScore().addScoreToBoxer1();
		
		// El Boxer 2 tuvo un conteo
		controller.getActualRoundScore().addCountToBoxer2();
		
		controller.endRound();
		RoundScore score = controller.getJudge().getCard().getRoundScore(1);
		
		boolean test = score.getScoreBoxer1() == 10d && score.getScoreBoxer2() == 8d;
		
		System.out.println("TEST testRoundWinBox1 => " + (test?"OK":"FAILED"));
	}
	
	public static void testRoundDraw(){
		
		controller.startRound();
		
		// El Boxer 1 dio 4 golpes
		controller.getActualRoundScore().addScoreToBoxer1();
		controller.getActualRoundScore().addScoreToBoxer1();
		controller.getActualRoundScore().addScoreToBoxer1();
		controller.getActualRoundScore().addScoreToBoxer1();
		
		// El Boxer 2 dio 2 golpes
		controller.getActualRoundScore().addScoreToBoxer2();
		controller.getActualRoundScore().addScoreToBoxer2();
		controller.getActualRoundScore().addScoreToBoxer2();
		controller.getActualRoundScore().addScoreToBoxer2();
		
		controller.endRound();
		RoundScore score = controller.getJudge().getCard().getRoundScore(2);
		
		boolean test = score.getScoreBoxer1() == 9d && score.getScoreBoxer2() == 9d;
		
		System.out.println("TEST testRoundDraw => " + (test?"OK":"FAILED"));
	}
	
	

}
