Feature: Play letter in hanged game
In order to play my play hanged game
As a user of the hanged API
I want to play a letter and know the result

Scenario: Letter is on phrase
Given category 'pelicula' with dictionary 'prueba'
When I start a new game
When I play the letter 'p'
Then I get the result word 'p _ _ _ _ _'
Then I get the played letters 'p'
Then I get 5 more chances to play

Scenario: Letter is not in phrase
Given category 'pelicula' with dictionary 'prueba'
When I start a new game
When I play the letter 'f'
Then I get the result word '_ _ _ _ _ _'
Then I get the played letters 'f'
Then I get 4 more chances to play