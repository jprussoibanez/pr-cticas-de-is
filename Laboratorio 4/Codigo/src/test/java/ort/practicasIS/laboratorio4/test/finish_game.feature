Feature: Finish game in hanged game
In order to finish playing hanged game
As a user of the hanged API
I want to know the status of the played game

Scenario: Win perfect hanged game
Given category 'pelicula' with dictionary 'terminator'
When I start a new game
When I play the letters 't e r m i n a t o r'
Then I get the result word 't e r m i n a t o r'
Then I win the game

Scenario: Lose perfect hanged game
Given category 'pelicula' with dictionary 'terminator'
When I start a new game
When I play the letters 'b d f g h'
Then I lose the game

Scenario: Win hanged game with mistakes
Given category 'pelicula' with dictionary 'terminator'
When I start a new game
When I play the letters 't e b r k m i n a t o r'
Then I win the game

Scenario: Lose hanged game with hits
Given category 'pelicula' with dictionary 'terminator'
When I start a new game
When I play the letters 'b p d l f g h'
Then I lose the game