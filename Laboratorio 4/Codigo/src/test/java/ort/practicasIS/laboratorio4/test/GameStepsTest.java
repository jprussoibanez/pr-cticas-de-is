package ort.practicasIS.laboratorio4.test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import junit.framework.Assert;
import ort.practicasIS.laboratorio4.model.Category;
import ort.practicasIS.laboratorio4.model.Game;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;

public class GameStepsTest {

	private Category category;
	private Game game;
	public GameStepsTest()
	{
	}
	
	@Given("^category '([a-zA-Z]*)' with dictionary '([a-zA-Z]*)'$")  
	public void categoryWithDictionary(String category, String word) {  
		this.category = new Category(category);
		this.category.addPhrase(word);
	}  
	 
	@When("^I start a new game$")  
	public void startNewGame() {  
		game = new Game(this.category);
		game.startNewGame();
	}
	
	@When("^I play the letter '([a-z _A-Z])'$")  
	public void playLetter(char letter) {  
		game.playLetter(letter);
	}
	
	@When("^I play the letters '([a-z _A-Z]*)'$")  
	public void playLetters(String letters) {  
		for (char letter : letters.toCharArray())
		{
			game.playLetter(letter);
		}
	}
	
	@Then("^I get the result word '([a-z _A-Z]*)'$")  
	public void resultWord(String resultWord) {  
		assertThat(game.getPlayedPhrase(), is(resultWord));
	}  
  
	@Then("^I get the played letters '([a-z A-Z]*)'$")  
	public void playedLetters(String playedLetters) {  
		assertThat(game.getPlayedLetters(), is(playedLetters));
	}
	
	@Then("^I get (\\d*) more chances to play$")  
	public void chancesLeft(int chancesLeft) {  
		assertThat(game.getChancesLeft(), is(chancesLeft));
	}
	
	@Then("^I win the game$")  
	public void winGame() {  
		assertThat(game.getGameStatus(), is(Game.GameStatus.WIN));
	}
	
	@Then("^I lose the game$")  
	public void loseGame() {  
		assertThat(game.getGameStatus(), is(Game.GameStatus.LOSE));
	}
}
