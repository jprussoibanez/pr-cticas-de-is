package ort.practicasIS.laboratorio4.model;

import java.util.*;
import java.util.regex.Pattern;

public class Game {

	private Category category;
	private String phrase;
	private List<Character> lettersPlayed;
	private int chancesLeft;
	public enum GameStatus {
		WIN,
		LOSE,
		PLAYING
	}
	public Game(Category category)
	{
		this.category = category;
		this.lettersPlayed = new ArrayList<Character>();
	}
	
	public String startNewGame()
	{
		this.chancesLeft = 5;
		lettersPlayed.clear();
		this.phrase = category.getRandomPhrase();
		return getPlayedPhrase();
	}
	
	public void playLetter(Character letter)
	{
		if (addLetterPlayed(letter))
		{
			if (!isLetterInPhrase(letter))
			{
				failLetter();
			}
		} else {
			failLetter();
		}
	}
	
	private void failLetter()
	{
		chancesLeft--;
	}
	
	private boolean isLetterInPhrase(Character letter)
	{
		return phrase.contains(letter.toString());
	}
	
	private boolean addLetterPlayed(Character letter)
	{
		if (!lettersPlayed.contains(letter))
		{
			lettersPlayed.add(letter);
			return true;
		} else {
			return false;
		}
	}
	public String getPlayedLetters()
	{
		String allLettersPlayed = "";
		for(Character letter : lettersPlayed)
		{
			allLettersPlayed += letter + " ";
		}
		return allLettersPlayed.trim();
	}
	
	public int getChancesLeft()
	{
		return chancesLeft;
	}

	public GameStatus getGameStatus()
	{
		if (isPhraseComplete())
		{
			return GameStatus.WIN;
		} else {
			if (chancesLeft > 0)
			{
				return GameStatus.PLAYING;
			} else {
				return GameStatus.LOSE;
			}
		}			
	}
	
	private boolean isPhraseComplete()
	{
		String playedPhrase = getPlayedPhrase();
		return !playedPhrase.contains("_");
	}
	
	private String addSpaces(String phrase)
	{
		String phraseWithSpaces = "";
		for (int i = 0; i < phrase.length(); i++)
		{
			phraseWithSpaces += phrase.charAt(i) + " ";
		}
		return phraseWithSpaces.trim();
	}
	
	public String getPlayedPhrase()
	{
		String completePhrase = addSpaces(phrase);
		StringBuilder constructPhrase = new StringBuilder(completePhrase); 
		for (int i = 0; i < completePhrase.length(); i++)
		{
			Character letter = completePhrase.charAt(i);
			if (!lettersPlayed.contains(letter) && !letter.equals(' ')){
				constructPhrase.setCharAt(i, '_');
			}
		}
		
		return constructPhrase.toString().trim();
	}
}
