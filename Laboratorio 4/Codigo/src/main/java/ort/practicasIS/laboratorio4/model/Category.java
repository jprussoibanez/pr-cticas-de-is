package ort.practicasIS.laboratorio4.model;
import java.util.*;

public class Category {
	String name;
	List<String> phrases;
	
	public Category(String name)
	{
		phrases = new ArrayList<String>();
	}
	
	public void addPhrase(String phrase)
	{
		phrases.add(phrase);
	}
	
	public String getRandomPhrase()
	{
		return phrases.get(0);
	}
}
