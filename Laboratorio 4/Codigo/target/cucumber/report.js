$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri('ort\practicasIS\laboratorio4\test\finish_game.feature');
formatter.feature({
  "id": "finish-game-in-hanged-game",
  "description": "In order to finish playing hanged game\r\nAs a user of the hanged API\r\nI want to know the status of the played game",
  "name": "Finish game in hanged game",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "finish-game-in-hanged-game;win-perfect-hanged-game",
  "description": "",
  "name": "Win perfect hanged game",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "category \u0027pelicula\u0027 with dictionary \u0027terminator\u0027",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I start a new game",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "I play the letters \u0027t e r m i n a t o r\u0027",
  "keyword": "When ",
  "line": 9
});
formatter.step({
  "name": "I get the result word \u0027t e r m i n a t o r\u0027",
  "keyword": "Then ",
  "line": 10
});
formatter.step({
  "name": "I win the game",
  "keyword": "Then ",
  "line": 11
});
formatter.match({
  "arguments": [
    {
      "val": "pelicula",
      "offset": 10
    },
    {
      "val": "terminator",
      "offset": 37
    }
  ],
  "location": "GameStepsTest.categoryWithDictionary(String,String)"
});
formatter.result({
  "duration": 140540778,
  "status": "passed"
});
formatter.match({
  "location": "GameStepsTest.startNewGame()"
});
formatter.result({
  "duration": 214793,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "t e r m i n a t o r",
      "offset": 20
    }
  ],
  "location": "GameStepsTest.playLetters(String)"
});
formatter.result({
  "duration": 94309,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "t e r m i n a t o r",
      "offset": 23
    }
  ],
  "location": "GameStepsTest.resultWord(String)"
});
formatter.result({
  "duration": 3175317,
  "status": "passed"
});
formatter.match({
  "location": "GameStepsTest.winGame()"
});
formatter.result({
  "duration": 772562,
  "status": "passed"
});
formatter.scenario({
  "id": "finish-game-in-hanged-game;lose-perfect-hanged-game",
  "description": "",
  "name": "Lose perfect hanged game",
  "keyword": "Scenario",
  "line": 13,
  "type": "scenario"
});
formatter.step({
  "name": "category \u0027pelicula\u0027 with dictionary \u0027terminator\u0027",
  "keyword": "Given ",
  "line": 14
});
formatter.step({
  "name": "I start a new game",
  "keyword": "When ",
  "line": 15
});
formatter.step({
  "name": "I play the letters \u0027b d f g h\u0027",
  "keyword": "When ",
  "line": 16
});
formatter.step({
  "name": "I lose the game",
  "keyword": "Then ",
  "line": 17
});
formatter.match({
  "arguments": [
    {
      "val": "pelicula",
      "offset": 10
    },
    {
      "val": "terminator",
      "offset": 37
    }
  ],
  "location": "GameStepsTest.categoryWithDictionary(String,String)"
});
formatter.result({
  "duration": 102777,
  "status": "passed"
});
formatter.match({
  "location": "GameStepsTest.startNewGame()"
});
formatter.result({
  "duration": 43113,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "b d f g h",
      "offset": 20
    }
  ],
  "location": "GameStepsTest.playLetters(String)"
});
formatter.result({
  "duration": 58895,
  "status": "passed"
});
formatter.match({
  "location": "GameStepsTest.loseGame()"
});
formatter.result({
  "duration": 73137,
  "status": "passed"
});
formatter.scenario({
  "id": "finish-game-in-hanged-game;win-hanged-game-with-mistakes",
  "description": "",
  "name": "Win hanged game with mistakes",
  "keyword": "Scenario",
  "line": 19,
  "type": "scenario"
});
formatter.step({
  "name": "category \u0027pelicula\u0027 with dictionary \u0027terminator\u0027",
  "keyword": "Given ",
  "line": 20
});
formatter.step({
  "name": "I start a new game",
  "keyword": "When ",
  "line": 21
});
formatter.step({
  "name": "I play the letters \u0027t e b r k m i n a t o r\u0027",
  "keyword": "When ",
  "line": 22
});
formatter.step({
  "name": "I win the game",
  "keyword": "Then ",
  "line": 23
});
formatter.match({
  "arguments": [
    {
      "val": "pelicula",
      "offset": 10
    },
    {
      "val": "terminator",
      "offset": 37
    }
  ],
  "location": "GameStepsTest.categoryWithDictionary(String,String)"
});
formatter.result({
  "duration": 176300,
  "status": "passed"
});
formatter.match({
  "location": "GameStepsTest.startNewGame()"
});
formatter.result({
  "duration": 77757,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "t e b r k m i n a t o r",
      "offset": 20
    }
  ],
  "location": "GameStepsTest.playLetters(String)"
});
formatter.result({
  "duration": 115095,
  "status": "passed"
});
formatter.match({
  "location": "GameStepsTest.winGame()"
});
formatter.result({
  "duration": 76217,
  "status": "passed"
});
formatter.scenario({
  "id": "finish-game-in-hanged-game;lose-hanged-game-with-hits",
  "description": "",
  "name": "Lose hanged game with hits",
  "keyword": "Scenario",
  "line": 25,
  "type": "scenario"
});
formatter.step({
  "name": "category \u0027pelicula\u0027 with dictionary \u0027terminator\u0027",
  "keyword": "Given ",
  "line": 26
});
formatter.step({
  "name": "I start a new game",
  "keyword": "When ",
  "line": 27
});
formatter.step({
  "name": "I play the letters \u0027b p d l f g h\u0027",
  "keyword": "When ",
  "line": 28
});
formatter.step({
  "name": "I lose the game",
  "keyword": "Then ",
  "line": 29
});
formatter.match({
  "arguments": [
    {
      "val": "pelicula",
      "offset": 10
    },
    {
      "val": "terminator",
      "offset": 37
    }
  ],
  "location": "GameStepsTest.categoryWithDictionary(String,String)"
});
formatter.result({
  "duration": 115095,
  "status": "passed"
});
formatter.match({
  "location": "GameStepsTest.startNewGame()"
});
formatter.result({
  "duration": 40418,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "b p d l f g h",
      "offset": 20
    }
  ],
  "location": "GameStepsTest.playLetters(String)"
});
formatter.result({
  "duration": 55045,
  "status": "passed"
});
formatter.match({
  "location": "GameStepsTest.loseGame()"
});
formatter.result({
  "duration": 76987,
  "status": "passed"
});
formatter.uri('ort\practicasIS\laboratorio4\test\play_letter.feature');
formatter.feature({
  "id": "play-letter-in-hanged-game",
  "description": "In order to play my play hanged game\r\nAs a user of the hanged API\r\nI want to play a letter and know the result",
  "name": "Play letter in hanged game",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "play-letter-in-hanged-game;letter-is-on-phrase",
  "description": "",
  "name": "Letter is on phrase",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "category \u0027pelicula\u0027 with dictionary \u0027prueba\u0027",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I start a new game",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "I play the letter \u0027p\u0027",
  "keyword": "When ",
  "line": 9
});
formatter.step({
  "name": "I get the result word \u0027p _ _ _ _ _\u0027",
  "keyword": "Then ",
  "line": 10
});
formatter.step({
  "name": "I get the played letters \u0027p\u0027",
  "keyword": "Then ",
  "line": 11
});
formatter.step({
  "name": "I get 5 more chances to play",
  "keyword": "Then ",
  "line": 12
});
formatter.match({
  "arguments": [
    {
      "val": "pelicula",
      "offset": 10
    },
    {
      "val": "prueba",
      "offset": 37
    }
  ],
  "location": "GameStepsTest.categoryWithDictionary(String,String)"
});
formatter.result({
  "duration": 4895200,
  "status": "passed"
});
formatter.match({
  "location": "GameStepsTest.startNewGame()"
});
formatter.result({
  "duration": 51196,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "p",
      "offset": 19
    }
  ],
  "location": "GameStepsTest.playLetter(char)"
});
formatter.result({
  "duration": 340281,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "p _ _ _ _ _",
      "offset": 23
    }
  ],
  "location": "GameStepsTest.resultWord(String)"
});
formatter.result({
  "duration": 196701,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "p",
      "offset": 26
    }
  ],
  "location": "GameStepsTest.playedLetters(String)"
});
formatter.result({
  "duration": 184768,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 6
    }
  ],
  "location": "GameStepsTest.chancesLeft(int)"
});
formatter.result({
  "duration": 520815,
  "status": "passed"
});
formatter.scenario({
  "id": "play-letter-in-hanged-game;letter-is-not-in-phrase",
  "description": "",
  "name": "Letter is not in phrase",
  "keyword": "Scenario",
  "line": 14,
  "type": "scenario"
});
formatter.step({
  "name": "category \u0027pelicula\u0027 with dictionary \u0027prueba\u0027",
  "keyword": "Given ",
  "line": 15
});
formatter.step({
  "name": "I start a new game",
  "keyword": "When ",
  "line": 16
});
formatter.step({
  "name": "I play the letter \u0027f\u0027",
  "keyword": "When ",
  "line": 17
});
formatter.step({
  "name": "I get the result word \u0027_ _ _ _ _ _\u0027",
  "keyword": "Then ",
  "line": 18
});
formatter.step({
  "name": "I get the played letters \u0027f\u0027",
  "keyword": "Then ",
  "line": 19
});
formatter.step({
  "name": "I get 4 more chances to play",
  "keyword": "Then ",
  "line": 20
});
formatter.match({
  "arguments": [
    {
      "val": "pelicula",
      "offset": 10
    },
    {
      "val": "prueba",
      "offset": 37
    }
  ],
  "location": "GameStepsTest.categoryWithDictionary(String,String)"
});
formatter.result({
  "duration": 170910,
  "status": "passed"
});
formatter.match({
  "location": "GameStepsTest.startNewGame()"
});
formatter.result({
  "duration": 71213,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "f",
      "offset": 19
    }
  ],
  "location": "GameStepsTest.playLetter(char)"
});
formatter.result({
  "duration": 76602,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "_ _ _ _ _ _",
      "offset": 23
    }
  ],
  "location": "GameStepsTest.resultWord(String)"
});
formatter.result({
  "duration": 96233,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "f",
      "offset": 26
    }
  ],
  "location": "GameStepsTest.playedLetters(String)"
});
formatter.result({
  "duration": 112401,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "4",
      "offset": 6
    }
  ],
  "location": "GameStepsTest.chancesLeft(int)"
});
formatter.result({
  "duration": 80836,
  "status": "passed"
});
formatter.uri('ort\practicasIS\laboratorio4\test\start_game.feature');
formatter.feature({
  "id": "start-game-in-hanged-game",
  "description": "In order to start playing hanged game\r\nAs a user of the hanged API\r\nI want to receive the phrase structure",
  "name": "Start game in hanged game",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "start-game-in-hanged-game;start-game-with-categories",
  "description": "",
  "name": "Start game with categories",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "category \u0027pelicula\u0027 with dictionary \u0027terminator\u0027",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I start a new game",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "I get the result word \u0027_ _ _ _ _ _ _ _ _ _\u0027",
  "keyword": "Then ",
  "line": 9
});
formatter.step({
  "name": "I get the played letters \u0027\u0027",
  "keyword": "Then ",
  "line": 10
});
formatter.step({
  "name": "I get 5 more chances to play",
  "keyword": "Then ",
  "line": 11
});
formatter.match({
  "arguments": [
    {
      "val": "pelicula",
      "offset": 10
    },
    {
      "val": "terminator",
      "offset": 37
    }
  ],
  "location": "GameStepsTest.categoryWithDictionary(String,String)"
});
formatter.result({
  "duration": 5534960,
  "status": "passed"
});
formatter.match({
  "location": "GameStepsTest.startNewGame()"
});
formatter.result({
  "duration": 73522,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "_ _ _ _ _ _ _ _ _ _",
      "offset": 23
    }
  ],
  "location": "GameStepsTest.resultWord(String)"
});
formatter.result({
  "duration": 105471,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 26
    }
  ],
  "location": "GameStepsTest.playedLetters(String)"
});
formatter.result({
  "duration": 62359,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 6
    }
  ],
  "location": "GameStepsTest.chancesLeft(int)"
});
formatter.result({
  "duration": 90075,
  "status": "passed"
});
});