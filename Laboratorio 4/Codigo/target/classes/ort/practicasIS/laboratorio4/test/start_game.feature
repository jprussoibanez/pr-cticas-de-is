Feature: Start game in hanged game
In order to start playing hanged game
As a user of the hanged API
I want to receive the phrase structure

Scenario: Start game with categories
Given category 'pelicula' with dictionary 'terminator'
When I start a new game
Then I get the result word '_ _ _ _ _ _ _ _ _ _'
Then I get the played letters ''
Then I get 5 more chances to play
