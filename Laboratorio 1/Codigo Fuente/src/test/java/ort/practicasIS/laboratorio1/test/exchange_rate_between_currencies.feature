Feature: Exchange rate between currencies
In order to operate with different currencies
As a user of the Money API
I want to convert between currencies with different exchange rate

Scenario: Exchange rate between two currencies
Given two currencies pesos and dollars with an exchange rate of 20
When I convert from 20 pesos to dollars
Then I get a new converted money of 400 dollars

Scenario: Exchange rate between same currencies
Given two currencies pesos and pesos with an exchange rate of 1
When I convert from 20 pesos to pesos
Then I get a new converted money of 20 pesos