package ort.practicasIS.laboratorio1.test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import junit.framework.Assert;
import ort.practicasIS.laboratorio1.model.Money;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;

public class MoneyStepsTest {

	private MoneyContext context;
	
	public MoneyStepsTest(MoneyContext context)
	{
		this.context = context;
	}
	
	@Given("^two money objects of (\\d*) ([a-z]*) and (\\d*) ([a-z]*)$")  
	public void createMoneyObjects(int firstMoneyAmount, String firstMoneyCurrency, int secondMoneyAmount, String secondMoneyCurrency) {  
		context.firstMoney = new Money(firstMoneyAmount,firstMoneyCurrency);
		context.secondMoney = new Money(secondMoneyAmount,secondMoneyCurrency);
	}  
	 
	@Given("^one money object of (\\d*) ([a-z]*)$")  
	public void createMoneyObjects(int moneyAmount, String moneyCurrency) {  
		context.firstMoney = new Money(moneyAmount,moneyCurrency);
	}
	
	@When("^I sum them up$")  
	public void sumThemUp() {  
	     context.resultMoney = context.firstMoney.sum(context.secondMoney);
	}  
  
	@When("^I substract them up$")  
	public void substractThemUp() {  
	     context.resultMoney = context.firstMoney.substract(context.secondMoney);
	}
	
	@When("^I multiple it (\\d*) times$")  
	public void multipleIt(int times) {  
	     context.resultMoney = context.firstMoney.multiple(times);
	}

	@When("^I divide it (\\d*) times$")  
	public void divideIt(int times) {  
	  context.resultMoney = context.firstMoney.divide(times);
	}
	
	@Then("^I get a new money of (-?\\d*) ([a-z]*)$")  
	public void getNewMoney(int amount, String currency) {  
		Money expectedMoney = new Money(amount,currency);
		assertThat(context.resultMoney, is(expectedMoney));  
	}
	
	@Then("^I get infinity$")  
	public void getInfinity() {  
		Assert.assertTrue(Double.isInfinite(context.resultMoney.getAmount()));  
	}
}
