package ort.practicasIS.laboratorio1.test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import ort.practicasIS.laboratorio1.model.ExchangeRate;
import ort.practicasIS.laboratorio1.model.Money;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;

public class ExchangeRateStepsTest {
	private MoneyContext context;
	
	public ExchangeRateStepsTest(MoneyContext context)
	{
		this.context = context;
	}

	@Given("^two currencies ([a-z]*) and ([a-z]*) with an exchange rate of (\\d*)$")  
	public void createExchangeRate(String firstMoneyCurrency, String secondMoneyCurrency, int exchangeRate) {  
		context.exchangeRate = new ExchangeRate(firstMoneyCurrency,secondMoneyCurrency,exchangeRate);
	}  
	   
	@When("^I convert from (\\d*) pesos to ([a-z]*)$")  
	public void convertPesosToDollars(int amount,String toCurrency) {  
	     context.resultMoney = context.exchangeRate.convert(new Money(amount,toCurrency));
	}  
  	
	@When("^I sum them up with the exchange rate$")  
	public void sumThemUpWithExchangeRate() {  
	     context.resultMoney = context.firstMoney.sum(context.secondMoney,context.exchangeRate);
	}
	
	@When("^I substract them up with the exchange rate$")  
	public void substractThemUpWithExchangeRate() {  
	     context.resultMoney = context.firstMoney.substract(context.secondMoney,context.exchangeRate);
	}
	
	@Then("^I get a new converted money of (\\d*) ([a-z]*)$")  
	public void getNewMoney(int amount, String currency) {  
		Money expectedMoney = new Money(amount,currency);
		assertThat(context.resultMoney, is(expectedMoney));  
	}
}