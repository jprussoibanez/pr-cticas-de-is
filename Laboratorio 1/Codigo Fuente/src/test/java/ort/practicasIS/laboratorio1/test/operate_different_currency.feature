Feature: Operate money of different currency
In order to operate with money of the different currency
As a user of the Money API
I want to operate (sum and subtract) money objects of different currency

Scenario: Sum two money objects of different currency
Given two money objects of 10 pesos and 1 dollars 
Given two currencies pesos and dollars with an exchange rate of 20
When I sum them up with the exchange rate
Then I get a new money of 30 pesos

Scenario: Subtract two positive money objects of different currency
Given two money objects of 20 pesos and 1 dollars 
Given two currencies pesos and dollars with an exchange rate of 10
When I substract them up with the exchange rate
Then I get a new money of 10 pesos

Scenario: Subtract two negative money objects of different currency
Given two money objects of 10 pesos and 2 dollars 
Given two currencies pesos and dollars with an exchange rate of 10
When I substract them up with the exchange rate
Then I get a new money of -10 pesos