package ort.practicasIS.laboratorio1.test;

import ort.practicasIS.laboratorio1.model.ExchangeRate;
import ort.practicasIS.laboratorio1.model.Money;

public class MoneyContext {

	public ExchangeRate exchangeRate;
	public Money firstMoney;
	public Money secondMoney;
	public Money resultMoney;
	public String exceptionName;
}
