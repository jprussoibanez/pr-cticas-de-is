Feature: Operate money of different currency
In order to operate with money of the different currency
As a user of the Money API
I want to operate (sum, subtract, multiple and divide) money objects of different currency

Scenario: Sum two money objects of different currency
Given two money objects $10 and USD1 
When I sum them up with an exchange rate of 20 pesos for one dollar
Then I get a new money $30