Feature: Operate money of same currency
In order to operate with money of the same currency
As a user of the Money API
I want to operate (sum, subtract, multiple and divide) money objects of same currency

Scenario: Sum two money objects
Given two money objects $10 and $15 
When I sum them up
Then I get a new money $25