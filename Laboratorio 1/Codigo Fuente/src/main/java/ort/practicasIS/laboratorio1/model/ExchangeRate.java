package ort.practicasIS.laboratorio1.model;

public class ExchangeRate {

	private String fromCurrency;
	private String toCurrency;
	private double rate;
	public ExchangeRate(String fromCurrency, String toCurrency, int rate)
	{
		this.fromCurrency = fromCurrency;
		this.toCurrency = toCurrency;
		this.rate = rate;
	}
	
	public Money convert(Money fromMoney)
	{
		return new Money(fromMoney.getAmount()*this.rate,toCurrency);
	}
}
