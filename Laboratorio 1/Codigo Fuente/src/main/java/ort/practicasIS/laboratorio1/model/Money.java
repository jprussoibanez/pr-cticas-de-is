package ort.practicasIS.laboratorio1.model;

public class Money {

	private double amount;
	private String currency;
	public Money(double amount, String currency)
	{
		this.amount = amount;
		this.currency = currency;
	}
	
	public boolean isSameCurrency(Money money)
	{
		return this.currency.equals(money.getCurrency());
	}
	public String getCurrency()
	{
		return this.currency;
	}
	
	public Money sum(Money money)
	{
		return new Money(this.amount + money.amount, this.currency);
	}

	public Money substract(Money money)
	{
		return new Money(this.amount - money.amount, this.currency);
	}

	public Money multiple(int times)
	{
		return new Money(this.amount * times, this.currency);
	}
	
	public Money divide(int times)
	{
		return new Money(this.amount / times, this.currency);
	}
	
	public Money sum(Money money, ExchangeRate exchangeRate)
	{
		return this.sum(exchangeRate.convert(money));
	}

	public Money substract(Money money, ExchangeRate exchangeRate)
	{
		return this.substract(exchangeRate.convert(money));
	}
	public double getAmount()
	{
		return this.amount;
	}
	
	public boolean equals(Object obj)
	{
		if (!(obj instanceof Money))
			return false;

		return ((Double)((Money)obj).amount).equals((Double)this.amount);
	}
	
	public String toString()
	{
		return this.amount + " " + this.currency;
	}
	
	public int hashCode() {
	  assert false : "hashCode not designed";
	  return 42; // any arbitrary constant will do
	}

}
