Feature: Operate money of same currency
In order to operate with money of the same currency
As a user of the Money API
I want to operate (sum, subtract, multiple and divide) money objects of same currency

Scenario: Sum two money objects
Given two money objects of 10 pesos and 15 pesos 
When I sum them up
Then I get a new money of 25 pesos

Scenario: Substract positive two money objects
Given two money objects of 15 pesos and 10 pesos 
When I substract them up
Then I get a new money of 5 pesos

Scenario: Substract negative two money objects
Given two money objects of 5 pesos and 10 pesos 
When I substract them up
Then I get a new money of -5 pesos

Scenario: Multiple a money objects
Given one money object of 10 pesos 
When I multiple it 4 times
Then I get a new money of 40 pesos

Scenario: Divide a money objects
Given one money object of 10 pesos 
When I divide it 2 times
Then I get a new money of 5 pesos

Scenario: Divide a money objects by zero
Given one money object of 10 pesos 
When I divide it 0 times
Then I get infinity