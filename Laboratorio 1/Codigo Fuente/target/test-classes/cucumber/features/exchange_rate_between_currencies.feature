Feature: Exchange rate between currencies
In order to operate with different currencies
As a user of the Money API
I want to convert between currencies with its exchange rate

Scenario: Exchange rate between two currencies
Given two currencies with exchange rate of 20
When I convert from pesos to dollars 
Then I get the exchange rate of 20 pesos for 1 dollar