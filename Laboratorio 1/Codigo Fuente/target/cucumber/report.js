$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri('ort\practicasIS\laboratorio1\test\exchange_rate_between_currencies.feature');
formatter.feature({
  "id": "exchange-rate-between-currencies",
  "description": "In order to operate with different currencies\r\nAs a user of the Money API\r\nI want to convert between currencies with different exchange rate",
  "name": "Exchange rate between currencies",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "exchange-rate-between-currencies;exchange-rate-between-two-currencies",
  "description": "",
  "name": "Exchange rate between two currencies",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "two currencies pesos and dollars with an exchange rate of 20",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I convert from 20 pesos to dollars",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "I get a new converted money of 400 dollars",
  "keyword": "Then ",
  "line": 9
});
formatter.match({
  "arguments": [
    {
      "val": "pesos",
      "offset": 15
    },
    {
      "val": "dollars",
      "offset": 25
    },
    {
      "val": "20",
      "offset": 58
    }
  ],
  "location": "ExchangeRateStepsTest.createExchangeRate(String,String,int)"
});
formatter.result({
  "duration": 95095177,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 15
    },
    {
      "val": "dollars",
      "offset": 27
    }
  ],
  "location": "ExchangeRateStepsTest.convertPesosToDollars(int,String)"
});
formatter.result({
  "duration": 172834,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 31
    },
    {
      "val": "dollars",
      "offset": 35
    }
  ],
  "location": "ExchangeRateStepsTest.getNewMoney(int,String)"
});
formatter.result({
  "duration": 2881205,
  "status": "passed"
});
formatter.scenario({
  "id": "exchange-rate-between-currencies;exchange-rate-between-same-currencies",
  "description": "",
  "name": "Exchange rate between same currencies",
  "keyword": "Scenario",
  "line": 11,
  "type": "scenario"
});
formatter.step({
  "name": "two currencies pesos and pesos with an exchange rate of 1",
  "keyword": "Given ",
  "line": 12
});
formatter.step({
  "name": "I convert from 20 pesos to pesos",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "I get a new converted money of 20 pesos",
  "keyword": "Then ",
  "line": 14
});
formatter.match({
  "arguments": [
    {
      "val": "pesos",
      "offset": 15
    },
    {
      "val": "pesos",
      "offset": 25
    },
    {
      "val": "1",
      "offset": 56
    }
  ],
  "location": "ExchangeRateStepsTest.createExchangeRate(String,String,int)"
});
formatter.result({
  "duration": 160901,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 15
    },
    {
      "val": "pesos",
      "offset": 27
    }
  ],
  "location": "ExchangeRateStepsTest.convertPesosToDollars(int,String)"
});
formatter.result({
  "duration": 38878,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 31
    },
    {
      "val": "pesos",
      "offset": 34
    }
  ],
  "location": "ExchangeRateStepsTest.getNewMoney(int,String)"
});
formatter.result({
  "duration": 45807,
  "status": "passed"
});
formatter.uri('ort\practicasIS\laboratorio1\test\operate_different_currency.feature');
formatter.feature({
  "id": "operate-money-of-different-currency",
  "description": "In order to operate with money of the different currency\r\nAs a user of the Money API\r\nI want to operate (sum and subtract) money objects of different currency",
  "name": "Operate money of different currency",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "operate-money-of-different-currency;sum-two-money-objects-of-different-currency",
  "description": "",
  "name": "Sum two money objects of different currency",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "two money objects of 10 pesos and 1 dollars",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "two currencies pesos and dollars with an exchange rate of 20",
  "keyword": "Given ",
  "line": 8
});
formatter.step({
  "name": "I sum them up with the exchange rate",
  "keyword": "When ",
  "line": 9
});
formatter.step({
  "name": "I get a new money of 30 pesos",
  "keyword": "Then ",
  "line": 10
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 24
    },
    {
      "val": "1",
      "offset": 34
    },
    {
      "val": "dollars",
      "offset": 36
    }
  ],
  "location": "MoneyStepsTest.createMoneyObjects(int,String,int,String)"
});
formatter.result({
  "duration": 2636390,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "pesos",
      "offset": 15
    },
    {
      "val": "dollars",
      "offset": 25
    },
    {
      "val": "20",
      "offset": 58
    }
  ],
  "location": "ExchangeRateStepsTest.createExchangeRate(String,String,int)"
});
formatter.result({
  "duration": 103546,
  "status": "passed"
});
formatter.match({
  "location": "ExchangeRateStepsTest.sumThemUpWithExchangeRate()"
});
formatter.result({
  "duration": 28485,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "30",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 24
    }
  ],
  "location": "MoneyStepsTest.getNewMoney(int,String)"
});
formatter.result({
  "duration": 53890,
  "status": "passed"
});
formatter.scenario({
  "id": "operate-money-of-different-currency;subtract-two-positive-money-objects-of-different-currency",
  "description": "",
  "name": "Subtract two positive money objects of different currency",
  "keyword": "Scenario",
  "line": 12,
  "type": "scenario"
});
formatter.step({
  "name": "two money objects of 20 pesos and 1 dollars",
  "keyword": "Given ",
  "line": 13
});
formatter.step({
  "name": "two currencies pesos and dollars with an exchange rate of 10",
  "keyword": "Given ",
  "line": 14
});
formatter.step({
  "name": "I substract them up with the exchange rate",
  "keyword": "When ",
  "line": 15
});
formatter.step({
  "name": "I get a new money of 10 pesos",
  "keyword": "Then ",
  "line": 16
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 24
    },
    {
      "val": "1",
      "offset": 34
    },
    {
      "val": "dollars",
      "offset": 36
    }
  ],
  "location": "MoneyStepsTest.createMoneyObjects(int,String,int,String)"
});
formatter.result({
  "duration": 239042,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "pesos",
      "offset": 15
    },
    {
      "val": "dollars",
      "offset": 25
    },
    {
      "val": "10",
      "offset": 58
    }
  ],
  "location": "ExchangeRateStepsTest.createExchangeRate(String,String,int)"
});
formatter.result({
  "duration": 93923,
  "status": "passed"
});
formatter.match({
  "location": "ExchangeRateStepsTest.substractThemUpWithExchangeRate()"
});
formatter.result({
  "duration": 26946,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 24
    }
  ],
  "location": "MoneyStepsTest.getNewMoney(int,String)"
});
formatter.result({
  "duration": 42727,
  "status": "passed"
});
formatter.scenario({
  "id": "operate-money-of-different-currency;subtract-two-negative-money-objects-of-different-currency",
  "description": "",
  "name": "Subtract two negative money objects of different currency",
  "keyword": "Scenario",
  "line": 18,
  "type": "scenario"
});
formatter.step({
  "name": "two money objects of 10 pesos and 2 dollars",
  "keyword": "Given ",
  "line": 19
});
formatter.step({
  "name": "two currencies pesos and dollars with an exchange rate of 10",
  "keyword": "Given ",
  "line": 20
});
formatter.step({
  "name": "I substract them up with the exchange rate",
  "keyword": "When ",
  "line": 21
});
formatter.step({
  "name": "I get a new money of -10 pesos",
  "keyword": "Then ",
  "line": 22
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 24
    },
    {
      "val": "2",
      "offset": 34
    },
    {
      "val": "dollars",
      "offset": 36
    }
  ],
  "location": "MoneyStepsTest.createMoneyObjects(int,String,int,String)"
});
formatter.result({
  "duration": 534284,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "pesos",
      "offset": 15
    },
    {
      "val": "dollars",
      "offset": 25
    },
    {
      "val": "10",
      "offset": 58
    }
  ],
  "location": "ExchangeRateStepsTest.createExchangeRate(String,String,int)"
});
formatter.result({
  "duration": 86609,
  "status": "passed"
});
formatter.match({
  "location": "ExchangeRateStepsTest.substractThemUpWithExchangeRate()"
});
formatter.result({
  "duration": 15782,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "-10",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 25
    }
  ],
  "location": "MoneyStepsTest.getNewMoney(int,String)"
});
formatter.result({
  "duration": 43112,
  "status": "passed"
});
formatter.uri('ort\practicasIS\laboratorio1\test\operate_same_currency.feature');
formatter.feature({
  "id": "operate-money-of-same-currency",
  "description": "In order to operate with money of the same currency\r\nAs a user of the Money API\r\nI want to operate (sum, subtract, multiple and divide) money objects of same currency",
  "name": "Operate money of same currency",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "operate-money-of-same-currency;sum-two-money-objects",
  "description": "",
  "name": "Sum two money objects",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "two money objects of 10 pesos and 15 pesos",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I sum them up",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "I get a new money of 25 pesos",
  "keyword": "Then ",
  "line": 9
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 24
    },
    {
      "val": "15",
      "offset": 34
    },
    {
      "val": "pesos",
      "offset": 37
    }
  ],
  "location": "MoneyStepsTest.createMoneyObjects(int,String,int,String)"
});
formatter.result({
  "duration": 2575955,
  "status": "passed"
});
formatter.match({
  "location": "MoneyStepsTest.sumThemUp()"
});
formatter.result({
  "duration": 24635,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "25",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 24
    }
  ],
  "location": "MoneyStepsTest.getNewMoney(int,String)"
});
formatter.result({
  "duration": 44652,
  "status": "passed"
});
formatter.scenario({
  "id": "operate-money-of-same-currency;substract-positive-two-money-objects",
  "description": "",
  "name": "Substract positive two money objects",
  "keyword": "Scenario",
  "line": 11,
  "type": "scenario"
});
formatter.step({
  "name": "two money objects of 15 pesos and 10 pesos",
  "keyword": "Given ",
  "line": 12
});
formatter.step({
  "name": "I substract them up",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "I get a new money of 5 pesos",
  "keyword": "Then ",
  "line": 14
});
formatter.match({
  "arguments": [
    {
      "val": "15",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 24
    },
    {
      "val": "10",
      "offset": 34
    },
    {
      "val": "pesos",
      "offset": 37
    }
  ],
  "location": "MoneyStepsTest.createMoneyObjects(int,String,int,String)"
});
formatter.result({
  "duration": 195160,
  "status": "passed"
});
formatter.match({
  "location": "MoneyStepsTest.substractThemUp()"
});
formatter.result({
  "duration": 23866,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 23
    }
  ],
  "location": "MoneyStepsTest.getNewMoney(int,String)"
});
formatter.result({
  "duration": 41572,
  "status": "passed"
});
formatter.scenario({
  "id": "operate-money-of-same-currency;substract-negative-two-money-objects",
  "description": "",
  "name": "Substract negative two money objects",
  "keyword": "Scenario",
  "line": 16,
  "type": "scenario"
});
formatter.step({
  "name": "two money objects of 5 pesos and 10 pesos",
  "keyword": "Given ",
  "line": 17
});
formatter.step({
  "name": "I substract them up",
  "keyword": "When ",
  "line": 18
});
formatter.step({
  "name": "I get a new money of -5 pesos",
  "keyword": "Then ",
  "line": 19
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 23
    },
    {
      "val": "10",
      "offset": 33
    },
    {
      "val": "pesos",
      "offset": 36
    }
  ],
  "location": "MoneyStepsTest.createMoneyObjects(int,String,int,String)"
});
formatter.result({
  "duration": 161671,
  "status": "passed"
});
formatter.match({
  "location": "MoneyStepsTest.substractThemUp()"
});
formatter.result({
  "duration": 14628,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "-5",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 24
    }
  ],
  "location": "MoneyStepsTest.getNewMoney(int,String)"
});
formatter.result({
  "duration": 43497,
  "status": "passed"
});
formatter.scenario({
  "id": "operate-money-of-same-currency;multiple-a-money-objects",
  "description": "",
  "name": "Multiple a money objects",
  "keyword": "Scenario",
  "line": 21,
  "type": "scenario"
});
formatter.step({
  "name": "one money object of 10 pesos",
  "keyword": "Given ",
  "line": 22
});
formatter.step({
  "name": "I multiple it 4 times",
  "keyword": "When ",
  "line": 23
});
formatter.step({
  "name": "I get a new money of 40 pesos",
  "keyword": "Then ",
  "line": 24
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 20
    },
    {
      "val": "pesos",
      "offset": 23
    }
  ],
  "location": "MoneyStepsTest.createMoneyObjects(int,String)"
});
formatter.result({
  "duration": 189771,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "4",
      "offset": 14
    }
  ],
  "location": "MoneyStepsTest.multipleIt(int)"
});
formatter.result({
  "duration": 36953,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "40",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 24
    }
  ],
  "location": "MoneyStepsTest.getNewMoney(int,String)"
});
formatter.result({
  "duration": 42342,
  "status": "passed"
});
formatter.scenario({
  "id": "operate-money-of-same-currency;divide-a-money-objects",
  "description": "",
  "name": "Divide a money objects",
  "keyword": "Scenario",
  "line": 26,
  "type": "scenario"
});
formatter.step({
  "name": "one money object of 10 pesos",
  "keyword": "Given ",
  "line": 27
});
formatter.step({
  "name": "I divide it 2 times",
  "keyword": "When ",
  "line": 28
});
formatter.step({
  "name": "I get a new money of 5 pesos",
  "keyword": "Then ",
  "line": 29
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 20
    },
    {
      "val": "pesos",
      "offset": 23
    }
  ],
  "location": "MoneyStepsTest.createMoneyObjects(int,String)"
});
formatter.result({
  "duration": 156667,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 12
    }
  ],
  "location": "MoneyStepsTest.divideIt(int)"
});
formatter.result({
  "duration": 38878,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 21
    },
    {
      "val": "pesos",
      "offset": 23
    }
  ],
  "location": "MoneyStepsTest.getNewMoney(int,String)"
});
formatter.result({
  "duration": 40418,
  "status": "passed"
});
formatter.scenario({
  "id": "operate-money-of-same-currency;divide-a-money-objects-by-zero",
  "description": "",
  "name": "Divide a money objects by zero",
  "keyword": "Scenario",
  "line": 31,
  "type": "scenario"
});
formatter.step({
  "name": "one money object of 10 pesos",
  "keyword": "Given ",
  "line": 32
});
formatter.step({
  "name": "I divide it 0 times",
  "keyword": "When ",
  "line": 33
});
formatter.step({
  "name": "I get infinity",
  "keyword": "Then ",
  "line": 34
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 20
    },
    {
      "val": "pesos",
      "offset": 23
    }
  ],
  "location": "MoneyStepsTest.createMoneyObjects(int,String)"
});
formatter.result({
  "duration": 146274,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0",
      "offset": 12
    }
  ],
  "location": "MoneyStepsTest.divideIt(int)"
});
formatter.result({
  "duration": 26561,
  "status": "passed"
});
formatter.match({
  "location": "MoneyStepsTest.getInfinity()"
});
formatter.result({
  "duration": 1410000,
  "status": "passed"
});
});